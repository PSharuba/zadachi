package by.sharuba.geometry.lab1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class Drawing extends JComponent {
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        //Drawing axis
        g.setColor(Color.black);
        g.drawRect(0,0,100,100);
        g.drawLine(0,400,800,400);
        g.drawLine(400,0,400,800);
        g.drawLine(800,400,795,395);
        g.drawLine(800,400,795,405);
        g.drawLine(400,0,395,5);
        g.drawLine(400,0,405,5);
        //End of axis
        int c=400;
        for (Point point : points) {
            g.setColor(point.color);
            g.drawString(point.name,point.x+c,point.y+c);
            g.drawOval(point.x+c,point.y+c,3,3);
        }
        for(Line line : lines) {
            g.setColor(line.color);
            g.drawString(line.name,line.x+c,line.y+c);
            g.drawLine(line.x+c,line.y+c,line.x2+c,line.y2+c);
        }
        for(Circle circle : circles){
            g.setColor(circle.color);
            g.drawString(circle.name,circle.x+c,circle.y+c);
            g.drawOval(circle.x+c-circle.radius/2,circle.y+c-circle.radius/2,circle.radius,circle.radius);
        }
    }

    public void clear() {
        points.clear();
        lines.clear();
        circles.clear();
        repaint();
    }
    //Start of points -----------------------------
    private final LinkedList<Point> points = new LinkedList<Point>();
    public void addPoint(String name,int x, int y, Color color) {
        points.add(new Point(name,x,y, color));
        repaint();
    }
    //End of points -------------------------------
    //Start of Lines ------------------------------
    private final LinkedList<Line> lines = new LinkedList<Line>();
    public void addLine(String name,int x1, int x2, int y1,int y2, Color color) {
        lines.add(new Line(name,x1,x2,y1,y2, color));
        repaint();
    }
    //End of lines --------------------------------
    //Start of circles ----------------------------
    private final LinkedList<Circle> circles = new LinkedList<Circle>();
    public void addCircle(String name,int x1, int x2, int radius, Color color) {
        circles.add(new Circle(name, x1, x2, radius, color));
        repaint();
    }
    //End of circles ------------------------------
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Drawing picture= new Drawing();
        picture.setPreferredSize(new Dimension(800, 800));
        mainFrame.getContentPane().add(picture,BorderLayout.CENTER);
        //UI elements
        JPanel buttonsPanel = new JPanel();
        JButton newLineButton = new JButton("New Line");
        JButton newPointButton = new JButton("New Point");
        JButton newCircleButton = new JButton("New Circle");
        JButton clearButton = new JButton("Clear");
        /*JButton addButton = new JButton("add");
        JTextField x1Field = new JTextField("x1");
        JTextField x2Field = new JTextField("x2");
        JTextField y1Field = new JTextField("y1");
        JTextField y2Field = new JTextField("y2");
        JTextField radiusField = new JTextField("radius");
        JTextField nameField = new JTextField("name");*/
        //
        buttonsPanel.add(newPointButton);
        buttonsPanel.add(newLineButton);
        buttonsPanel.add(newCircleButton);
        buttonsPanel.add(clearButton);
        /*buttonsPanel.add(x1Field);
        buttonsPanel.add(x2Field);
        buttonsPanel.add(y1Field);
        buttonsPanel.add(y2Field);
        buttonsPanel.add(radiusField);
        buttonsPanel.add(nameField);
        buttonsPanel.add(addButton);*/
        //

        //
        mainFrame.getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
        newLineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

               /* String name=nameField.getText();
                int x1= Integer.parseInt(x1Field.getText());
                int x2= Integer.parseInt(x2Field.getText());
                int y1= Integer.parseInt(y1Field.getText());
                int y2= Integer.parseInt(y2Field.getText());
                Color randomColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
                picture.addLine(name,x1, y1, x2, y2, randomColor);
                x1Field.setVisible(false);
                x2Field.setVisible(false);
                y1Field.setVisible(false);
                y2Field.setVisible(false);*/
                int x1 = (int) (Math.random()*800-400);
                int x2 = (int) (Math.random()*800-400);
                int y1 = (int) (Math.random()*800-400);
                int y2 = (int) (Math.random()*800-400);
                Color randomColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
                picture.addLine("Line",x1, y1, x2, y2, randomColor);
            }
        });
        newPointButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int y = (int) (Math.random()*800-400);
                int x = (int) (Math.random()*800-400);
                Color randomColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
                picture.addPoint("Point",x, y, randomColor);
            }
        });
        newCircleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int x = (int) (Math.random()*800-400);
                int y = (int) (Math.random()*800-400);
                int radius = (int) (Math.random()*100);
                Color randomColor = new Color((float)Math.random(), (float)Math.random(), (float)Math.random());
                picture.addCircle("Circle",x, y, radius, randomColor);
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                picture.clear();
            }
        });
        mainFrame.pack();
        mainFrame.setVisible(true);
    }
}
