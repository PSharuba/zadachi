package by.sharuba.geometry.lab1;

import java.awt.*;

public class Circle extends Point{
    protected int radius;
    public Circle(){
        super("Circle");
    }
    public Circle(String name,int x,int y,int radius){
        super(name,x,y);
        this.radius=radius;
    }
    public Circle(String name, int x, int y, int radius, Color color){
        super(name,x,y,color);
        this.radius=radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    @Override
    public String toString(){
        return ("Circle "+name+"\nCenter: ( "+x+" , "+y+" )\nRadius: "+radius+'\n');
    }
}
