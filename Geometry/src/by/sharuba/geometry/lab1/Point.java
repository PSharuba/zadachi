package by.sharuba.geometry.lab1;

import java.awt.*;

public class Point {
    protected String name;
    protected int x;
    protected int y;
    protected Color color;

    public Point(String name, int x, int y, Color color) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.color = color;
    }
    public Point(){
        name="Point";
        x=0;
        y=0;
        color=Color.BLACK;
    }
    public Point(String name){
        this.name=name;
        x=0;
        y=0;
        color=Color.black;
    }
    public Point(String name, int x,int y){
        this.name= name;
        this.x=x;
        this.y=y;
        color=Color.black;
    }

    /*public void drawPoint(){
         * (0,0) -левый верхний угол;
         * (0+x,0+y) - смещение вправо вниз при x,y>0;
         * Пусть поле 1001х1001px,
         * тогда центр поля - (501,501)

    }*/

    public int[] getCoordinates(){
        return new int[]{x, y};
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Point "+name+":\nCoordinates: ( "+x+" ; "+y+" )\nColor: "+color+'\n';
    }
}
