package by.sharuba.geometry.lab1;

import java.awt.*;

public class Line extends Point{
    protected int x2;
    protected int y2;

    public Line(){
        super("Line");
    }
    public Line(String name,int x1,int x2,int y1,int y2,Color color){
        super(name,x1,y1,color);
        this.x2=x2;
        this.y2=y2;
    }
    public Line(String name,int x1,int x2,int y1,int y2){
        super(name,x1,y1);
        this.x2=x2;
        this.y2=y2;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
    @Override
    public String toString(){
        return "Line "+name+":\nCoordinates: ( "+x+" , "+y+"; "
                +x2+" , "+y2+" )\nColor: "+color+'\n';
    }
}
