import java.util.ArrayList;

public class Functions {
    public static double[] substract(Point p1, Point p2) {
        double[] vector = new double[2];
        vector[0] = p1.x - p2.x;
        vector[1] = p1.y - p2.y;
        return vector;
    }
    public static double isLeft(Point p0, Point[] line) {
        return (line[1].x - line[0].x) * (p0.y - line[0].y) - (p0.x - line[0].x) * (line[1].y - line[0].y);
    }
    public static boolean lineIntersections(Point[] line1, Point[] line2) {
        double[] v13 = substract(line2[0], line1[0]);
        double[] v31 = substract(line1[0], line2[0]);
        double[] v23 = substract(line2[0], line1[1]);
        double[] v32 = substract(line1[1], line2[0]);
        double[] v14 = substract(line2[1], line1[0]);
        double[] v41 = substract(line1[0], line2[1]);
        double[] v24 = substract(line2[1], line1[1]);
        double[] v42 = substract(line1[1], line2[1]);
        double ang1 = v13[0] * v14[0] + v13[1] * v14[1];
        double ang2 = v23[0] * v24[0] + v23[1] * v24[1];
        double ang3 = v31[0] * v32[0] + v31[1] * v32[1];
        double ang4 = v41[0] * v42[0] + v41[1] * v42[1];
        double det1 = isLeft(line2[0], line1);
        double det2 = isLeft(line2[1], line1);
        double det3 = isLeft(line1[0], line2);
        double det4 = isLeft(line1[1], line2);
        if (det1 == 0 && det2 == 0 && det3 == 0 && det4 == 0) {
            return (ang1 <= 0 || ang2 <= 0 || ang3 <= 0 || ang4 <= 0);
        }
        if ((det1 * det2) > 0 || (det3 * det4) > 0) {
            return false;
        }
        return ((det1 * det2) <= 0 && (det3 * det4) <= 0);
    }

    public static boolean simplePolynom(ArrayList<Point> poly) {
        Point[] polynom=poly.toArray(new Point[0]);
        for (int j = 2; j < polynom.length - 2; j++) {
            if (lineIntersections(new Point[]{polynom[0], polynom[1]}, new Point[]{polynom[j], polynom[j + 1]}))
                return false;
        }
        for (int i = 1; i < polynom.length - 3; i++) {
            for (int j = i + 2; j < polynom.length - 1; j++)
                if (lineIntersections(new Point[]{polynom[i], polynom[i + 1]}, new Point[]{polynom[j], polynom[j + 1]}))
                    return false;
        }
        return true;
    }

    public static boolean convex(ArrayList<Point> poly) {
        if (!simplePolynom(poly)) return false;
        Point[] polynom=poly.toArray(new Point[0]);
        double orientation = 0;
        for (int i = 0; i < polynom.length - 1; i++) {
            if ((orientation = isLeft(polynom[i + 2], new Point[]{polynom[i], polynom[i + 1]})) != 0)
                break;
        }
        for (int i = 0; i < polynom.length - 2; i++) {
            if (isLeft(polynom[i + 2], new Point[]{polynom[i], polynom[i + 1]}) * orientation < 0)
                return false;
        }
        if ((isLeft(polynom[1], new Point[]{polynom[polynom.length - 2], polynom[polynom.length - 1]}) * orientation) < 0)
            return false;
        return true;
    }
    public static boolean polyIntersection(ArrayList<Point> poly1,ArrayList<Point> poly2){
        Point[] polynom1=poly1.toArray(new Point[0]);
        Point[] polynom2=poly2.toArray(new Point[0]);
        for(int i=0;i<polynom1.length-1;i++)
            for(int j=0;j<polynom2.length-1;j++) {
                Point[] line1=new Point[]{polynom1[i],polynom1[i+1]};
                Point[] line2=new Point[]{polynom2[j],polynom2[j+1]};
                if (lineIntersections(line1,line2)) return true;
            }
        return false;
    }
}
