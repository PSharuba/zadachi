import java.util.ArrayList;

public class MainOp {
    public int[] dimensions(ArrayList<Point> args){
        Point[] arr=args.toArray(new Point[args.size()]);
        int minX=arr[0].x;
        int minY=arr[0].y;
        int maxX=arr[0].x;
        int maxY=arr[0].y;
        for (Point point: args) {
            if (point.x < minX) minX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.x > maxX) maxX = point.x;
            if (point.y > maxY) maxY = point.y;
        }
        return new int[]{minX,minY,maxX,maxY};
    }
    public boolean dimensionsTest(int[] dims,Point p0){
        return (p0.x>dims[2] || p0.x<dims[0] || p0.y>dims[3] || p0.y<dims[1]);
    }
    public int octan(Point p,Point p0){
        int x=p.x-p0.x;
        int y=p.y-p0.y;
        if(x==0 && y==0) return 0;
        if(y>=0 && x>y) return 1;
        if(x>0 && y>=x) return 2;
        if(x<=0 && x>-y) return 3;
        if(y>0 && y<=-x) return 4;
        if(y<=0 && x<y) return 5;
        if(y<=x && x<0) return 6;
        if(x>=0 && x<-y) return 7;
        else return 8;
    }
    public int[] difference(int[] oct){
        int[] D=new int[oct.length-1];
        for (int i=0;i<D.length;i++)
            D[i]=oct[i+1]-oct[i];
        return D;
    }
    public int[] correction(int[] D,Point[] p,Point p0){
        for(int i=0;i<D.length;i++) {
            if (D[i] > 4) D[i] -= 8;
            if (D[i] < -4) D[i] += 8;
            if (D[i] == 4 || D[i] == -4) {
                int aX = p[i].x - p0.x;
                int aY = p[i].y - p0.y;
                int bX = p[i+1].x - p0.x;
                int bY = p[i+1].y - p0.y;
                int d = (aX * bY - aY * bX);
                if (d < 0) D[i]=-4;
                if (d > 0) D[i]= 4;
                if (d ==0) D[i]= 10;
            }
        }
        return D;
    }
    public boolean onEdge(int []D){
        for(int d: D){
            if(d==10) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        ArrayList<Point> polygonOuter = new ArrayList<>();
        ArrayList<Point> polygonInner = new ArrayList<>();
        while (true) {
            boolean f1=true;
            boolean f2=true;
            while (f1) {
                polygonOuter.clear();
                for (int i = 0; i < 14; i++) {
                    int x = 0;
                    int y = 0;
                    while (Math.abs(x) < 10) x = (int) (Math.random() * 15);
                    while (Math.abs(y) < 5) y = (int) (Math.random() * 10);
                    polygonOuter.add(new Point(x, y));
                }
                polygonOuter.add(polygonOuter.get(0));
                f1=!Functions.simplePolynom(polygonOuter);
            }
            while (f2){
            polygonInner.clear();
            for (int i = 0; i < 14; i++){
                int x=(int)(Math.random()*10);
                int y=(int)(Math.random()*5);
                polygonInner.add(new Point(x,y));
            }
            polygonInner.add(polygonInner.get(0));
            f2=!Functions.simplePolynom(polygonInner);
            }
            if(!Functions.polyIntersection(polygonOuter,polygonInner)) break;
        }
        /*flag=true;
        while (flag) {
            polygonInner.clear();
            for (int i = 0; i < 14; i++){
                int x=(int)(Math.random()*10);
                int y=(int)(Math.random()*5);
                polygonInner.add(new Point(x,y));
            }
            polygonInner.add(polygonInner.get(0));
            if(!Functions.simplePolynom(polygonOuter) && !Functions.polyIntersection(polygonOuter,polygonInner)) flag=false;
        }*/
        System.out.println("+");
        /*polygon.add(new Point(-6,4));
        polygon.add(new Point(-4,-2));
        polygon.add(new Point(4,1));
        polygon.add(new Point(7,1));
        polygon.add(new Point(2,5));
        polygon.add(new Point(-2,3));
        polygon.add(new Point(-6,4));
        MainOp mainOp =new MainOp(0,0);
        mainOp.dimensions(polygon);*/
        /*if(mainOp.dimensionsTest()) {
            System.out.println("Point outside (dim test)");
            System.exit(0);
        }
        int[] octans=new int[polygon.size()];
        for (int i=0;i<octans.length;i++) {
            octans[i] = mainOp.octan(polygon.get(i));
            if(octans[i]==0){
                System.out.println("Point in vertex");
                System.exit(0);
            }
        }
        int[] dif=mainOp.difference(octans);
        dif=mainOp.correction(dif,polygon.toArray(new Point[polygon.size()]));
        if(mainOp.onEdge(dif)) {
            System.out.println("Point on edge");
            System.exit(0);
        }
        int s=0;
        for(int d: dif) s+=d;
        if(s==8 || s==-8) {
            System.out.println("Point inside (S=" +s+")");
            System.exit(0);
        }
        if(s==0) System.out.println("Point outside (S=" +s+")");
        else System.out.println("Error\n(S=" +s+")");*/
    }
}
