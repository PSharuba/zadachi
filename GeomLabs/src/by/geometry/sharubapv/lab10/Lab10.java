package by.geometry.sharubapv.lab10;

import by.geometry.sharubapv.lab3.DoublePoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Lab10 extends JComponent implements ActionListener {
    private Timer timer;
    private Color color=new Color(0,0,0);

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    Lab10() {
        timer = new Timer(5, this);
        setPreferredSize(new Dimension(400, 400));
    }

    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    public static ArrayList<DoublePoint> points = new ArrayList<>();

    private boolean outOfBounds(DoublePoint p) {
        return (Math.abs(p.x) > 200 || Math.abs(p.y) > 200);
    }

    private void turnAround() {
        int r=(int)(Math.random()*255);
        int g=(int)(Math.random()*255);
        int b=(int)(Math.random()*255);
        color=new Color(r,g,b);
        for (DoublePoint p : points) {
            p.angle += Math.random() * Math.PI / 2 + Math.PI / 4;
            p.x += Math.cos(p.angle) * 3;
            p.y += Math.sin(p.angle) * 3;
        }
    }

    private void turnAround(DoublePoint p) {
        p.angle += Math.random() * Math.PI / 3 + Math.PI / 6;
        p.x += Math.cos(p.angle) * 5;
        p.y += Math.sin(p.angle) * 5;
    }

    private double length(DoublePoint a, DoublePoint b) {
        double[] v = new double[]{a.x - b.x, a.y - b.y};
        return Math.sqrt(v[0] * v[0] + v[1] * v[1]);
    }

    private double minDistance() {
        ArrayList<DoublePoint> X = (ArrayList) points.clone();
        ArrayList<DoublePoint> Y = (ArrayList) points.clone();
        boolean needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < X.size(); i++) {
                if (X.get(i).x > X.get(i - 1).x) {
                    DoublePoint tmp = X.get(i);
                    X.set(i, X.get(i - 1));
                    X.set(i - 1, tmp);
                    needIteration = true;
                }
            }
        }
        needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < Y.size(); i++) {
                if (Y.get(i).y > Y.get(i - 1).y) {
                    DoublePoint tmp = Y.get(i);
                    Y.set(i, Y.get(i - 1));
                    Y.set(i - 1, tmp);
                    needIteration = true;
                }
            }
        }
        return distance(X, Y);
    }

    private double distance(ArrayList<DoublePoint> X, ArrayList<DoublePoint> Y) {
        if (X.size() == 3) {
            double AB = length(X.get(0), X.get(1));
            double BC = length(X.get(2), X.get(1));
            double AC = length(X.get(2), X.get(0));
            if (AB >= BC && AB >= AC) return AB;
            else if (BC > AC) return BC;
            else return AC;
        }
        if (X.size() == 2) {
            return length(X.get(0), X.get(1));
        }
        if(X.size()==1){
            return 500;
        }
        int sep = X.size() / 2;
        ArrayList<DoublePoint> Xl = new ArrayList<>();
        ArrayList<DoublePoint> Xr = new ArrayList<>();
        ArrayList<DoublePoint> Yl = new ArrayList<>();
        ArrayList<DoublePoint> Yr = new ArrayList<>();
        for (int i = 0; i < sep; i++)
            Xl.add(X.get(i));
        for (int i = sep + 1; i < X.size(); i++)
            Xr.add(X.get(i));
        for (DoublePoint p : Y) {
            if (p.x < X.get(sep).x)
                Yl.add(p);
            else Yr.add(p);
        }
        double Dl = distance(Xl, Yl);
        double Dr = distance(Xr, Yr);
        double d = Math.min(Dl, Dr);
        ArrayList<DoublePoint> Yd = new ArrayList<>();
        for (DoublePoint p : Y) {
            if (Math.abs(X.get(sep).x - p.x) <= d)
                Yd.add(p);
        }
        for (int i = 0; i < Yd.size() - 1; i++) {
            for (int j = i + 1; j < Yd.size() && j - i <= 7; j++) {
                double dist = length(Yd.get(i), Yd.get(j));
                if (dist < d)
                    d = dist;
            }
        }
        return d;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        int c = 200;
        g.setColor(color);
        for (DoublePoint p : points)
            g.fillOval((int) p.x + c - 30, c - (int) p.y - 30, 60, 60);
        if (minDistance() <= 60)
            turnAround();
        for (DoublePoint p : points) {
            p.x += Math.cos(p.angle);
            p.y += Math.sin(p.angle);
            if (outOfBounds(p))
                turnAround(p);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (int i = 0; i < 5; i++)
                    points.add(new DoublePoint((int) (Math.random() * 400 - 200), (int) (Math.random() * 400 - 200)));
                JFrame frame = new JFrame("Lab 10");
                JPanel panel = new JPanel();
                final Lab10 g = new Lab10();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(600, 500);
                frame.setVisible(true);
            }
        });
    }
}
