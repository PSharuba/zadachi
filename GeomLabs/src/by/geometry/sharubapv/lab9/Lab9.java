package by.geometry.sharubapv.lab9;

import by.geometry.sharubapv.lab3.DoublePoint;
import by.geometry.sharubapv.lab3.Functions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Comparator;

public class Lab9 extends JComponent implements ActionListener {
    private Timer timer;
    private static ArrayList<DoublePoint> P;
    private static ArrayList<DoublePoint> Q;
    private static ArrayList<DoublePoint> I;
    private static boolean shuffledP = false;
    private static boolean shuffledQ = false;

    private void start() {
        timer.start();
    }

    private void stop() {
        timer.stop();
    }

    private Lab9() {
        timer = new Timer(10, this);
        setPreferredSize(new Dimension(1000, 1000));
        P = new ArrayList<>();
        Q = new ArrayList<>();
        I = new ArrayList<>();
        generateConvexPoly();
    }

    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    /*========================================================================*/

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.GREEN);
        for (int i = 0; i < P.size() - 2; i++) {
            g.drawString(""+i, (int) P.get(i).x-2, (int) P.get(i).y);
            g2.draw(new Line2D.Double(P.get(i).x, P.get(i).y, P.get(i + 1).x, P.get(i + 1).y));
        }
        g2.setColor(Color.BLUE);
        for (int i = 0; i < Q.size() - 2; i++) {
            g.drawString(""+i, (int) Q.get(i).x-2, (int) Q.get(i).y);
            g2.draw(new Line2D.Double(Q.get(i).x, Q.get(i).y, Q.get(i + 1).x, Q.get(i + 1).y));
        }
        g2.setColor(Color.RED);
        intersection();
        for (int i = 0; i < I.size() - 1; i++) {
            g2.draw(new Line2D.Double(I.get(i).x, I.get(i).y, I.get(i + 1).x, I.get(i + 1).y));
            g2.drawOval((int)I.get(i).x-5,(int) I.get(i).y-5,10,10);
        }
        for (int i = 0; i < P.size() - 2; i++) {
            P.get(i).x++;
            Q.get(i).x--;
        }
        if (P.get(0).x > 700) generateConvexPoly();
    }

    /*========================================================================*/

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Lab 6");
                JPanel panel = new JPanel();
                final Lab9 g = new Lab9();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(1100, 1000);
                frame.setVisible(true);
            }
        });
    }

    /*========================================================================*/

    private void generateConvexPoly() {
        shuffledP=false;
        shuffledQ=false;
        P.clear();
        Q.clear();
        for (int i = 0; i < 4; i++) {
            P.add(new DoublePoint(300, 500));
            P.get(P.size() - 1).x += Math.cos(P.get(P.size() - 1).angle) * 200;
            P.get(P.size() - 1).y += Math.sin(P.get(P.size() - 1).angle) * 200;
            Q.add(new DoublePoint(700, 500));
            Q.get(Q.size() - 1).x += Math.cos(Q.get(Q.size() - 1).angle) * 240;
            Q.get(Q.size() - 1).y += Math.sin(Q.get(Q.size() - 1).angle) * 240;
        }
        P.sort(Comparator.naturalOrder());
        P.add(P.get(0));
        P.add(P.get(1));
        Q.sort(Comparator.naturalOrder());
        Q.add(Q.get(0));
        Q.add(Q.get(1));
    }

    private void shuffle(int startI, int startJ) {
        if(startI!=0) {
            P.remove(P.size() - 1);
            P.remove(P.size() - 1);
            ArrayList<DoublePoint> newArrP = new ArrayList<>();
            for (int i = startI; i < P.size(); i++) {
                newArrP.add(P.get(i));
            }
            for (int i = 0; i < startI; i++) {
                newArrP.add(P.get(i));
            }
            P = newArrP;
            P.add(P.get(0));
            P.add(P.get(1));
        }
        if(startJ!=0) {
            Q.remove(Q.size() - 1);
            Q.remove(Q.size() - 1);
            ArrayList<DoublePoint> newArrQ = new ArrayList<>();
            for (int i = startJ; i < Q.size(); i++) {
                newArrQ.add(Q.get(i));
            }
            for (int i = 0; i < startJ; i++) {
                newArrQ.add(Q.get(i));
            }
            Q = newArrQ;
            Q.add(Q.get(0));
            Q.add(Q.get(1));
        }
    }

    private DoublePoint intersectionPoint(DoublePoint p11, DoublePoint p12, DoublePoint p21, DoublePoint p22) {
        double a1 = p11.y - p12.y;
        double b1 = p12.x - p11.x;
        double a2 = p21.y - p22.y;
        double b2 = p22.x - p21.x;
        double c1 = p12.y * p11.x - p12.x * p11.y;
        double c2 = p22.y * p21.x - p22.x * p21.y;
        double d = a1 * b2 - a2 * b1;
        double x = (b1 * c2 - b2 * c1) / d;
        double y = (a2 * c1 - a1 * c2) / d;
        return new DoublePoint(x, y);
    }

    private void intersection() {
        I.clear();
        boolean flag = false;
        int i = 0;
        int j = 0;
        for (; i < P.size() - 1; i++) {
            for (j=0; j < Q.size() - 1; j++) {
                if (lineIntersectQ(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1))) {
                    I.add(intersectionPoint(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)));
                    flag = true;
                    break;
                }
            }
            if (flag) break;
        }
        if (!flag) return;
        shuffle(i,j);
        i = 0;
        j = 0;
        while (i < P.size() - 1 && j < Q.size() - 1) {
            if (
                    looks(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)) &&
                            looks(Q.get(j), Q.get(j + 1), P.get(i+1), P.get(i))
            ) {
                if (outerWindowP(P.get(i + 1), Q.get(j), Q.get(j + 1))) {
                    //I.add(Q.get(j + 1));
                    //I.add(P.get(i + 1));
                    i++;
                } else {
                    //I.add(P.get(i + 1));
                    //I.add(Q.get(j + 1));
                    j++;
                }
                continue;
            }
            if (
                    looks(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)) &&
                            !looks(Q.get(j), Q.get(j + 1), P.get(i+1), P.get(i))
            ) {
                if (!outerWindowP(P.get(i + 1), Q.get(j), Q.get(j + 1)))
                    I.add(P.get(i + 1));
                i++;
                continue;
            }
            if (
                    !looks(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)) &&
                            looks(Q.get(j), Q.get(j + 1), P.get(i+1), P.get(i))
            ) {
                if (!outerWindowP(Q.get(j + 1), P.get(i), P.get(i + 1)))
                    I.add(Q.get(j + 1));
                j++;
                continue;
            }
            if (
                    !looks(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)) &&
                            !looks(Q.get(j), Q.get(j + 1), P.get(i), P.get(i + 1))
            ) {
                if (lineIntersectQ(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1))) {
                    I.add(intersectionPoint(P.get(i), P.get(i + 1), Q.get(j), Q.get(j + 1)));
                }
                if (outerWindowP(P.get(i + 1), Q.get(j), Q.get(j + 1)))
                    i++;
                else j++;
            }
        }
        I.add(I.get(0));
    }

    private boolean outerWindowP(DoublePoint p2, DoublePoint q1, DoublePoint q2) {
        // p2
        return Functions.isLeft(p2, new DoublePoint[]{q1, q2}) < 0;
    }

    private boolean looks(DoublePoint a1, DoublePoint a2, DoublePoint b1, DoublePoint b2) {
        // 1 looks on 2 if
        double[] b1b2 = new double[]{b2.x - b1.x, b2.y - b1.y};
        double[] a1a2 = new double[]{a2.x - a1.x, a2.y - a1.y};
        double[] b1a2 = new double[]{a2.x - b1.x, a2.y - b1.y};

        /*if (a1a2[0] * b1b2[1] - b1b2[0] * a1a2[1] >= 0)
            return Functions.isLeft(a2, new DoublePoint[]{b1, b2}) >= 0;*/
        double d1=b1b2[0]*a1a2[1]-b1b2[1]*a1a2[0];
        double d2=b1b2[0]*b1a2[1]-b1b2[1]*b1a2[0];
        if(d1>0 && d2<0) return true;
        if(d1<0 && d2>0) return true;
        return false;
    }
        /*(va.x * vb.y) >= (vb.x * va.y)
        double x=this.y*v.z-this.z*v.y;
        double y=this.z*v.x-this.x*v.z;
        double z=this.x*v.y-this.y*v.x;*/


        /*double[] b1a2 = new double[]{p12.x - p21.x, p12.y - p21.y};
        double d1 = b1b2[0] * a1a2[1] - b1b2[1] * a1a2[0];

        if(d1>0 && Functions.isLeft(p12,new DoublePoint[]{p21,p22})>=0) return true;
        if(d1<0 && Functions.isLeft(p12,new DoublePoint[]{p21,p22})<=0) return true;
        return false;
        /*double d2 = b1b2[0] * b1a2[1] - b1b2[1] * b1a2[0];
        return (d1 < 0 && d2 > 0) || (d1 > 0 && d2 < 0);

        //If[Det[{B[[2]] - B[[1]], A[[2]] - A[[1]]}] < 0 &&
        //  Det[{B[[2]] - B[[1]], A[[2]] - B[[1]]}] > 0, flag = True];
        //If[Det[{B[[2]] - B[[1]], A[[2]] - A[[1]]}] > 0 &&
        //  Det[{B[[2]] - B[[1]], A[[2]] - B[[1]]}] < 0, flag = True];


        /*double cos1 = cos(p11, p12, p21);
        double cos2 = cos(p11, p12, p22);
        if (cos1 == 0 || cos2 == 0) {
            double a = p11.y - p12.y;
            double b = p12.x - p11.x;
            double c = p12.y * p11.x - p12.x * p11.y;
            return a * p22.x + b * p22.y + c == 0
                    ||
                    a * p21.x + b * p21.y + c == 0;
        }*/

    /*
    IsIntersectSecondQ[p1_, p2_, p3_, p4_] :=
            If[Det[{p4 - p3, p1 - p3}]*Det[{p4 - p3, p2 - p3}] <= 0, True, False];
    IsIntersectQ[p1_, p2_, p3_, p4_] :=
            If[Det[{p2 - p1, p3 - p1}]*Det[{p2 - p1, p4 - p1}] <= 0,
                    IsIntersectSecondQ[p1, p2, p3, p4], False];
     */
    private boolean lineIntersectQ(DoublePoint p1, DoublePoint p2, DoublePoint p3, DoublePoint p4){
        double d1=(p2.x-p1.x)*(p3.y-p1.y)-(p2.y-p1.y)*(p3.x-p1.x);
        double d2=(p2.x-p1.x)*(p4.y-p1.y)-(p2.y-p1.y)*(p4.x-p1.x);
        if(d1*d2>0) return false;
        double d3=(p4.x-p3.x)*(p1.y-p3.y)-(p4.y-p3.y)*(p1.x-p3.x);
        double d4=(p4.x-p3.x)*(p2.y-p3.y)-(p4.y-p3.y)*(p2.x-p3.x);
        return d4*d3<=0;
    }

//    private boolean lineIntersectQ(DoublePoint p1, DoublePoint p2, DoublePoint p3, DoublePoint p4) {
//        // A B C D
//        //z= v1.x*v2.y-v1.y*v2.x      AC*AD   BC*BD
//        double[] AC = new double[]{p3.x - p1.x, p3.y - p1.y};
//        double[] AD = new double[]{p4.x - p1.x, p4.y - p1.y};
//        double[] BC = new double[]{p3.x - p2.x, p3.y - p2.y};
//        double[] BD = new double[]{p4.x - p2.x, p4.y - p2.y};
//        double za = AC[0] * AD[1] - AC[1] * AD[0];
//        double zb = BC[0] * BD[1] - BC[1] * BD[0];
//        if (za * zb > 0) return false;
//        double[] CA = new double[]{p1.x - p3.x, p1.y - p3.y};
//        double[] CB = new double[]{p2.x - p3.x, p2.y - p3.y};
//        double[] DA = new double[]{p1.x - p4.x, p1.y - p4.y};
//        double[] DB = new double[]{p2.x - p4.x, p2.y - p4.y};
//        double zc = CA[0] * CB[1] - CA[1] * CB[0];
//        double zd = DA[0] * DB[1] - DA[1] * DB[0];
//        if (zc * zd < 0 && za * zb < 0) return true;
//        if (za == 0 && cos(p1, p3, p4) < 0) return true;
//        if (zb == 0 && cos(p2, p3, p4) < 0) return true;
//        if (zc == 0 && cos(p3, p1, p2) < 0) return true;
//        return zd == 0 && cos(p4, p1, p2) < 0;
//    }

}