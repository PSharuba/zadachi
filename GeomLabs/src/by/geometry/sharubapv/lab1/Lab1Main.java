package by.geometry.sharubapv.lab1;

import java.util.ArrayList;

import by.geometry.sharubapv.Functions.*;

public class Lab1Main {
    public static void main(String[] args) {
        ArrayList<Point> polynom = new ArrayList<>();
        polynom.add(new Point(0, 0));
        polynom.add(new Point(7, 10));
        polynom.add(new Point(10, 3));
        polynom.add(new Point(10, 5));
        polynom.add(new Point(5, -2));
        polynom.add(new Point(0, 0));
        if (Functions.simplePolynom(polynom.toArray(new Point[0]))) {
            System.out.println("simple");
        } else System.out.println("not simple");
        if (Functions.convex(polynom.toArray(new Point[0]))) {
            System.out.println("convex");
        } else System.out.println("not convex");
    }
}
