package by.geometry.sharubapv.tests;

import by.geometry.sharubapv.lab3.DoublePoint;

public class LookTest {
    public static boolean looks(DoublePoint a1, DoublePoint a2, DoublePoint b1, DoublePoint b2) {
        // 1 looks on 2 if
        double[] b1b2 = new double[]{b2.x - b1.x, b2.y - b1.y};
        double[] a1a2 = new double[]{a2.x - a1.x, a2.y - a1.y};
        double[] b1a2 = new double[]{a2.x - b1.x, a2.y - b1.y};

        /*if (a1a2[0] * b1b2[1] - b1b2[0] * a1a2[1] >= 0)
            return Functions.isLeft(a2, new DoublePoint[]{b1, b2}) >= 0;*/
        double d1=b1b2[0]*a1a2[1]-b1b2[1]*a1a2[0];
        double d2=b1b2[0]*b1a2[1]-b1b2[1]*b1a2[0];
        if(d1>0 && d2<0) return true;
        if(d1<0 && d2>0) return true;
        return false;
    }

    public static void main(String[] args) {

    }
}
