package by.geometry.sharubapv.tests;

import by.geometry.sharubapv.lab11.Point;

public class Triangle {
    public Point A;
    public Point B;
    public Point C;

    private double isLeft(Point p0, Point[] line) {
        return (line[1].x - line[0].x) * (p0.y - line[0].y) - (p0.x - line[0].x) * (line[1].y - line[0].y);
        /*
        det<0 - right
        det>0 - left
        det=0 - on line
         */
    }
    public Triangle(Point a, Point b, Point c) {
        if(isLeft(c,new Point[]{a,b})>0){
            A=a;
            B=b;
            C=c;
        }else{
            A=b;
            B=a;
            C=c;
        }
    }
    /*public Point findDiff(Triangle t){
    }*/

    public void flip(Triangle t){

    }
}
