package by.geometry.sharubapv.lab3;

import java.util.Objects;

public class DoublePoint implements Comparable<DoublePoint> {
    public double x;
    public double y;
    public double angle;
    public DoublePoint(){
        x=0;
        y=0;
        angle=0;
    }

    public DoublePoint( double x, double y) {
        this.x = x;
        this.y = y;
        generateAngle();
    }
    public void generateAngle(){
        angle=Math.random()*Math.PI*2;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoublePoint that = (DoublePoint) o;
        return Double.compare(that.x, x) == 0 &&
                Double.compare(that.y, y) == 0 &&
                Double.compare(that.angle, angle) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, angle);
    }

    @Override
    public int compareTo(DoublePoint o){
        if(this.angle>o.angle) return 1;
        if(this.angle<o.angle) return -1;
        return 0;
    }
}
