package by.geometry.sharubapv.lab3;

import java.util.ArrayList;

public class Functions {
    public static boolean polyIntersection(ArrayList<DoublePoint> poly1, ArrayList<DoublePoint> poly2) {
        DoublePoint[] polynom1 = poly1.toArray(new DoublePoint[0]);
        DoublePoint[] polynom2 = poly2.toArray(new DoublePoint[0]);
        for (int i = 0; i < polynom1.length - 1; i++)
            for (int j = 0; j < polynom2.length - 1; j++) {
                DoublePoint[] line1 = new DoublePoint[]{polynom1[i], polynom1[i + 1]};
                DoublePoint[] line2 = new DoublePoint[]{polynom2[j], polynom2[j + 1]};
                if (lineIntersections(line1, line2)) return true;
            }
        return false;
    }

    public static boolean lineIntersections(DoublePoint[] line1, DoublePoint[] line2) {
        double[] v13 = substract(line2[0], line1[0]);
        double[] v31 = substract(line1[0], line2[0]);
        double[] v23 = substract(line2[0], line1[1]);
        double[] v32 = substract(line1[1], line2[0]);
        double[] v14 = substract(line2[1], line1[0]);
        double[] v41 = substract(line1[0], line2[1]);
        double[] v24 = substract(line2[1], line1[1]);
        double[] v42 = substract(line1[1], line2[1]);
        double ang1 = v13[0] * v14[0] + v13[1] * v14[1];
        double ang2 = v23[0] * v24[0] + v23[1] * v24[1];
        double ang3 = v31[0] * v32[0] + v31[1] * v32[1];
        double ang4 = v41[0] * v42[0] + v41[1] * v42[1];
        double det1 = isLeft(line2[0], line1);
        double det2 = isLeft(line2[1], line1);
        double det3 = isLeft(line1[0], line2);
        double det4 = isLeft(line1[1], line2);
        if (det1 == 0 && det2 == 0 && det3 == 0 && det4 == 0) {
            return (ang1 <= 0 || ang2 <= 0 || ang3 <= 0 || ang4 <= 0);
        }
        if ((det1 * det2) > 0 || (det3 * det4) > 0) {
            return false;
        }
        return ((det1 * det2) <= 0 && (det3 * det4) <= 0);
    }

    public static double[] substract(DoublePoint p1, DoublePoint p2) {
        double[] vector = new double[2];
        vector[0] = p1.x - p2.x;
        vector[1] = p1.y - p2.y;
        return vector;
    }

    public static double isLeft(DoublePoint p0, DoublePoint[] line) {
        return (line[1].x - line[0].x) * (p0.y - line[0].y) - (p0.x - line[0].x) * (line[1].y - line[0].y);
        /*
        det<0 - right
        det>0 - left
        det=0 - on line
         */
    }

    public static boolean simplePolynom(ArrayList<DoublePoint> poly) {
        DoublePoint[] polynom = poly.toArray(new DoublePoint[0]);
        for (int j = 2; j < polynom.length - 2; j++) {
            if (lineIntersections(new DoublePoint[]{polynom[0], polynom[1]}, new DoublePoint[]{polynom[j], polynom[j + 1]}))
                return false;
        }
        for (int i = 1; i < polynom.length - 3; i++) {
            for (int j = i + 2; j < polynom.length - 1; j++)
                if (lineIntersections(new DoublePoint[]{polynom[i], polynom[i + 1]}, new DoublePoint[]{polynom[j], polynom[j + 1]}))
                    return false;
        }
        return true;
    }

    public static boolean convex(ArrayList<DoublePoint> poly) {
        DoublePoint[] polynom = poly.toArray(new DoublePoint[0]);
        if (!simplePolynom(poly)) return false;
        double orientation = 0;
        for (int i = 0; i < polynom.length - 1; i++) {
            if ((orientation = isLeft(polynom[i + 2], new DoublePoint[]{polynom[i], polynom[i + 1]})) != 0)
                break;
        }
        for (int i = 0; i < polynom.length - 2; i++) {
            if (isLeft(polynom[i + 2], new DoublePoint[]{polynom[i], polynom[i + 1]}) * orientation < 0)
                return false;
        }
        if ((isLeft(polynom[1], new DoublePoint[]{polynom[polynom.length - 2], polynom[polynom.length - 1]}) * orientation) < 0)
            return false;
        return true;
    }

    //OCTAN TEST

    public static double[] dimensions(ArrayList<DoublePoint> args) {
        DoublePoint[] arr = args.toArray(new DoublePoint[0]);
        double minX = arr[0].x;
        double minY = arr[0].y;
        double maxX = arr[0].x;
        double maxY = arr[0].y;
        for (DoublePoint point : args) {
            if (point.x < minX) minX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.x > maxX) maxX = point.x;
            if (point.y > maxY) maxY = point.y;
        }
        return new double[]{minX, minY, maxX, maxY};
    }

    public static double maxCoord(ArrayList<DoublePoint> args) {
        DoublePoint[] arr = args.toArray(new DoublePoint[0]);
        double minX = arr[0].x;
        double minY = arr[0].y;
        double maxX = arr[0].x;
        double maxY = arr[0].y;
        for (DoublePoint point : args) {
            if (point.x < minX) minX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.x > maxX) maxX = point.x;
            if (point.y > maxY) maxY = point.y;
        }
        double[] dims = new double[]{minX, minY, maxX, maxY};
        double m = dims[0];
        for (double d : dims)
            if (d > m) m = d;
        return m;
    }

    public static boolean dimensionsTest(double[] dims, DoublePoint p0) {
        return (p0.x > dims[2] || p0.x < dims[0] || p0.y > dims[3] || p0.y < dims[1]);
    }

    public static int octan(DoublePoint p, DoublePoint p0) {
        double x = p.x - p0.x;
        double y = p.y - p0.y;
        if (x == 0 && y == 0) return 0;
        if (y >= 0 && x > y) return 1;
        if (x > 0 && y >= x) return 2;
        if (x <= 0 && x > -y) return 3;
        if (y > 0 && y <= -x) return 4;
        if (y <= 0 && x < y) return 5;
        if (y <= x && x < 0) return 6;
        if (x >= 0 && x < -y) return 7;
        else return 8;
    }

    public static int[] difference(int[] oct) {
        int[] D = new int[oct.length - 1];
        for (int i = 0; i < D.length; i++)
            D[i] = oct[i + 1] - oct[i];
        return D;
    }

    public static int[] correction(int[] D, DoublePoint[] p, DoublePoint p0) {
        for (int i = 0; i < D.length; i++) {
            if (D[i] > 4) D[i] -= 8;
            if (D[i] < -4) D[i] += 8;
            if (D[i] == 4 || D[i] == -4) {
                double aX = p[i].x - p0.x;
                double aY = p[i].y - p0.y;
                double bX = p[i + 1].x - p0.x;
                double bY = p[i + 1].y - p0.y;
                double d = (aX * bY - aY * bX);
                if (d < 0) D[i] = -4;
                if (d > 0) D[i] = 4;
                if (d == 0) D[i] = 10;
            }
        }
        return D;
    }

    public static boolean onEdge(int[] D) {
        for (int d : D) {
            if (d == 10) return true;
        }
        return false;
    }

    public static boolean octanTest(DoublePoint p0, ArrayList<DoublePoint> poly) {
        if (dimensionsTest(dimensions(poly), p0)) {
            return false;
        }
        int[] octans = new int[poly.size()];
        for (int i = 0; i < octans.length; i++) {
            octans[i] = octan(poly.get(i), p0);
            if (octans[i] == 0) {
                return true;
            }
        }
        int[] dif = difference(octans);
        dif = correction(dif, poly.toArray(new DoublePoint[0]), p0);
        if (onEdge(dif)) {
            return true;
        }
        int s = 0;
        for (int d : dif) s += d;
        return (s == 8 || s == -8);
    }
}
