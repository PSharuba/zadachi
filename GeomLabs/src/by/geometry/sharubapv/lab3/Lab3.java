package by.geometry.sharubapv.lab3;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class Lab3 extends JComponent implements ActionListener {
    private double scale;
    private Color color;
    private Timer timer;
    private ArrayList<DoublePoint> points;
    private ArrayList<DoublePoint> polygonInner;
    private ArrayList<DoublePoint> polygonOuter;

    public static DoublePoint nextPosition(DoublePoint p0) {
        DoublePoint p=new DoublePoint();
        p.x =p0.x + Math.cos(p0.angle)*10;
        p.y =p0.y + Math.sin(p0.angle)*10;
        p.angle=p0.angle;
        return p;
    }

    public void generatePolygons() {
        while (true) {
            for (int i = 0; i < 10; i++) {
                double x = (Math.random() * 400 - 200);
                double y = (Math.random() * 400 - 200);
                polygonInner.add(new DoublePoint(x, y));
            }
            polygonInner.add(polygonInner.get(0));
            if (Functions.simplePolynom(polygonInner)) break;
            polygonInner.clear();
        }
        polygonOuter.add(new DoublePoint(450, 150));
        polygonOuter.add(new DoublePoint(450, -200));
        polygonOuter.add(new DoublePoint(400, -450));
        polygonOuter.add(new DoublePoint(-300, -400));
        polygonOuter.add(new DoublePoint(-350, -150));
        polygonOuter.add(new DoublePoint(-400, 200));
        polygonOuter.add(new DoublePoint(-250, 400));
        polygonOuter.add(new DoublePoint(150, 450));
        polygonOuter.add(polygonOuter.get(0));
    }

    public Lab3(Color color, int delay) {
        scale = 1.0;
        timer = new Timer(delay, this);
        this.color = color;
        setPreferredSize(new Dimension(1000, 1000));
        points = new ArrayList<>();
        polygonInner = new ArrayList<>();
        polygonOuter = new ArrayList<>();
    }

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    private boolean pointToConvexPoly(DoublePoint p0,ArrayList<DoublePoint> poly){
        DoublePoint z=new DoublePoint();
        z.x=(poly.get(0).x+poly.get(poly.size()/2).x)/2;
        z.y=(poly.get(0).y+poly.get(poly.size()/2).y)/2;
        int start=0;
        int end=poly.size()-1;
        while (end-start>1){
            int sect=(start+end)/2;
            if(angle2(poly.get(0),z,poly.get(sect))<=angle2(poly.get(0),z,p0))
                end=sect;
            else start=sect;
        }
        return (Functions.isLeft(p0,new DoublePoint[]{poly.get(start),poly.get(end)})<=0);

    }

    private double angle2(DoublePoint p1,DoublePoint z,DoublePoint p2){
        double[] v1=new double[]{p1.x-z.x,p1.y-z.y};
        double[] v2=new double[]{p2.x-z.x,p2.y-z.y};
        double ca=(p1.x-z.x)*(p2.x-z.x)+(p1.y-z.y)*(p2.y-z.y);
        ca/=Math.sqrt(v1[0]*v1[0]+v1[1]*v1[1])*Math.sqrt(v2[0]*v2[0]+v2[1]*v2[1]);
        ca=Math.acos(ca);
        if(Functions.isLeft(p2,new DoublePoint[]{z,p1})<0)
            ca=6.28-ca;
        return ca;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        int c = 450;
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.white);
        int width = 1000;
        int height = 1000;
        g.fillRect(0, 0, width, height);
        g2d.setColor(Color.black);
        g2d.drawRect(0, 0, width + 1, height + 1);
        for (int i = 0; i < polygonInner.size() - 1; i++) {
            g.drawLine((int) polygonInner.get(i).x + c, c - (int) polygonInner.get(i).y, c + (int) polygonInner.get(i + 1).x, c - (int) polygonInner.get(i + 1).y);
        }
        for (int i = 0; i < polygonOuter.size() - 1; i++) {
            g.drawLine((int) polygonOuter.get(i).x + c, c - (int) polygonOuter.get(i).y, c + (int) polygonOuter.get(i + 1).x, c - (int) polygonOuter.get(i + 1).y);
        }
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(color);
        g2d.scale(scale, scale);
        if (points.size() == 0) {
            for(int i=0;i<5000;i++){
                DoublePoint p=new DoublePoint(0,0);
                while(!Functions.octanTest(p,polygonOuter) || Functions.octanTest(p,polygonInner)){          //Functions.octanTest(p,polygonInner) || !Functions.octanTest(p,polygonOuter)){
                    p.x=Math.random()*900-c;
                    p.y=Math.random()*900-c;
                }
                points.add(p);
            }
            for (DoublePoint p : points)
                p.generateAngle();
        }
        for (int i = 0; i < points.size(); ) {
            DoublePoint p = points.get(i);
            hit(points.get(i), polygonOuter);
            if (Functions.octanTest(new DoublePoint(p.x, p.y), polygonInner)) {
                points.remove(i);
                continue;
            }
            p.x += Math.cos(p.angle)*3;
            p.y += Math.sin(p.angle)*3;
            Ellipse2D el = new Ellipse2D.Double(p.x + c-10, c - p.y-10, 20, 20);
            g2d.fill(el);
            i++;
        }
    }

    private void hit(DoublePoint p, ArrayList<DoublePoint> poly) {
        DoublePoint[] polygon = poly.toArray(new DoublePoint[0]);
        int i;
        double[] a = new double[]{-p.x + nextPosition(p).x, -p.y + nextPosition(p).y};

        for (i = 0; i < polygon.length - 1; i++) {
            if (Functions.lineIntersections(new DoublePoint[]{p, nextPosition(p)},
                    new DoublePoint[]{polygon[i], polygon[i + 1]})) {
                double[] b = new double[]{polygon[i + 1].x - polygon[i].x, polygon[i + 1].y - polygon[i].y};
                double ab = scalar(a, b);
                double bb = scalar(b, b);
                double koef = 2 * ab / bb;
                double[] a2 = new double[]{koef * b[0] - a[0], koef * b[1] - a[1]};
                p.angle = angle(a2);
                hit(p,poly);
                return;
            }
        }

    }

    private double scalar(@NotNull double[] a, @NotNull double[] b) {
        return a[0] * b[0] + a[1] * b[1];
    }

    private double angle(double[] v) {
        double cos = scalar(v, new double[]{1, 0})/(Math.sqrt(v[0]*v[0]+v[1]*v[1]));
        if (v[1] < 0) return 6.28 - Math.acos(cos);
        else return Math.acos(cos);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Lab 3");
                JPanel panel = new JPanel();
                final Lab3 g = new Lab3(Color.green, 10);
                g.generatePolygons();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(1000, 1000);
                frame.setVisible(true);
            }
        });
    }
}
