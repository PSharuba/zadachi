package by.geometry.sharubapv.lab11;

import java.util.Objects;

public class Point {
    public int x;
    public int y;
    public Point(){
        x=(int) (Math.random() * 600 - 300);
        y=(int) (Math.random() * 600 - 300);
    }

    public Point( int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
