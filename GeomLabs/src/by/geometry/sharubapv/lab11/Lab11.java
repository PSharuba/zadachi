package by.geometry.sharubapv.lab11;


import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Lab11 extends JFrame {
    private DrawingPane pane;
    private ArrayList<Point> points;
    private ArrayList<Point> triangleSides;

    public static double isLeft(Point p0, Point[] line) {
        return (line[1].x - line[0].x) * (p0.y - line[0].y) - (p0.x - line[0].x) * (line[1].y - line[0].y);
        /*
        det<0 - right
        det>0 - left
        det=0 - on line
         */
    }

    public static int findMinXY(ArrayList<Point> arr) {
        int ymin = Integer.MAX_VALUE;
        int xmin;
        int imin;
        ArrayList<Point> minPoints = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).y < ymin) {
                minPoints.clear();
                ymin = arr.get(i).y;
                minPoints.add(arr.get(i));
            }
            if (arr.get(i).y == ymin)
                minPoints.add(arr.get(i));
        }
        xmin = minPoints.get(0).x;
        if (minPoints.size() > 1)
            for (int i = 1; i < minPoints.size(); i++) {
                if (minPoints.get(i).x >= xmin) {
                    minPoints.remove(minPoints.get(i));
                    i--;
                } else {
                    xmin = minPoints.get(i).x;
                }
            }
        imin = arr.indexOf(minPoints.get(0));
        return imin;
    }

    private boolean exists(Point a, Point b, Point c, ArrayList<Point> arr) {
        for (int i = 0; i < arr.size() - 3; i++) {
            ArrayList<Point> sides=new ArrayList<>();
            sides.add(arr.get(i));
            sides.add(arr.get(i+1));
            sides.add(arr.get(i+2));
            for(Point p:sides)
                if(p==a) {
                    sides.remove(p);
                    break;
                }
            for(Point p:sides)
                if(p==b) {
                    sides.remove(p);
                    break;
                }
            for(Point p:sides)
                if(p==c) {
                    sides.remove(p);
                    break;
                }
            if(sides.size()==0) return true;
        }
        return false;
    }

    private double cos(Point p0, Point p1, Point p2) {
        double[] v1 = new double[]{p1.x - p0.x, p1.y - p0.y};
        double[] v2 = new double[]{p2.x - p0.x, p2.y - p0.y};
        double len1 = Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
        double len2 = Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
        return (v1[0] * v2[0] + v1[1] * v2[1]) / (len1 * len2);
    }

    private void findNext(Point p1, Point p2) {
        ArrayList<Point> leftSet = new ArrayList<>();
        for (Point p : points) {
            if (isLeft(p, new Point[]{p1, p2}) > 0)
                leftSet.add(p);
        }
        if (leftSet.isEmpty()) return;
        Point next = leftSet.get(0);
        double maxAngle = 2;
        for (Point p : leftSet) {
            if (cos(p, p1, p2) < maxAngle) {
                maxAngle = cos(p, p1, p2);
                next = p;
            }
        }
        if (exists(p1, next, p2, triangleSides)) {
            /*triangleSides.add(p1);
            triangleSides.add(next);
            triangleSides.add(next);
            triangleSides.add(p2);*/
            return;
        }

        triangleSides.add(p1);
        triangleSides.add(next);
        triangleSides.add(next);
        triangleSides.add(p2);
        findNext(p1, next);
        findNext(next, p2);
    }


    private void Delane() {
        generatePoints();
        triangleSides.clear();
        Point p1 = points.get(findMinXY(points));
        points.remove(p1);
        Point p2 = points.get(findMinXY(points));
        points.add(p1);
        if (p2.x < p1.x) {
            Point tmp = p1;
            p1 = p2;
            p2 = tmp;
        }
        triangleSides.add(p1);
        triangleSides.add(p2);
        findNext(p1, p2);
    }

    public Lab11() {
        points = new ArrayList<>();
        triangleSides = new ArrayList<>();
        generatePoints();
        setTitle("PointDraw");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pane = new DrawingPane();
        JPanel topPanel = new JPanel();
        JButton clearButton = new JButton("Remake");
        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pane.repaint();
            }
        });
        topPanel.add(clearButton);

        add(pane);
        add(topPanel, BorderLayout.NORTH);
        pack();
    }

    class DrawingPane extends JComponent {
        public DrawingPane() {
            setPreferredSize(new Dimension(1000, 1000));
            setBorder(new LineBorder(Color.GRAY));
        }

        @Override
        protected void paintComponent(Graphics g) {
            g.clearRect(-100, -100, 2000, 2000);
            int c = 500;
            int r = 5;
            super.paintComponent(g);
            Delane();
            for (Point p : points) {
                g.fillOval(c + p.x - r, c - p.y - r, 2 * r, 2 * r);
            }
            g.setColor(Color.GREEN);
            g.fillOval(c + triangleSides.get(0).x - r, c - triangleSides.get(0).y - r, 2 * r, 2 * r);
            g.fillOval(c + triangleSides.get(1).x - r, c - triangleSides.get(1).y - r, 2 * r, 2 * r);
            g.setColor(Color.BLACK);
            for (int i = 0; i < triangleSides.size() - 1; i += 2) {
                g.drawLine(c + triangleSides.get(i).x, c - triangleSides.get(i).y, c + triangleSides.get(i + 1).x, c - triangleSides.get(i + 1).y);
            }

        }
    }

    private void generatePoints() {
        points.clear();
        for (int i = 0; i < 100; i++) {
            points.add(new Point());
        }
    }

    public static void main(String[] args) {
        new Lab11().setVisible(true);
    }
}