package by.geometry.sharubapv.lab12;

public class Cube {
    private Point3D[] p;

    public Cube(Point3D point, Vector3D v1, Vector3D v2, double length) {
        v1.norm();
        v2.norm();
        Vector3D v11 = v1.mult((int) length);
        Vector3D v21 = v2.mult((int) length);
        //v1*v2 vector
        Vector3D v3 = v1.vectProd(v2).mult((int) length);
        Point3D p1= point.addVector(v11);
        Point3D p2= point.addVector(v21);
        Point3D p3= point.addVector(v3);
        Point3D p4=p2.addVector(v11);
        Point3D p5=p2.addVector(v3);
        Point3D p6=p1.addVector(v3);
        Point3D p7=p5.addVector(v11);
        p=new Point3D[]{point,p1,p2,p3,p4,p5,p6,p7};
    }
    private void rotate(Vector3D rotate_vector, Point3D rotate_point) {

        //RV - относительно чего вращение
        // RP - что вращаем

        rotate_vector.norm();

        double rotate_angle = Math.PI / Math.pow(2,7);

        double qw = Math.cos(rotate_angle / 2);
        double qx = rotate_vector.x * Math.sin(rotate_angle / 2);
        double qy = rotate_vector.y * Math.sin(rotate_angle / 2);
        double qz = rotate_vector.z * Math.sin(rotate_angle / 2);
        double[] RV=new double[]{qw,qx,qy,qz};

        double pw=0;
        double px=rotate_point.x;
        double py=rotate_point.y;
        double pz=rotate_point.z;
        double[] RP=new double[]{pw,px,py,pz};

        double[] newQofPoint = multQuat(multQuat(RV,RP),revQuat(RV));
        rotate_point.x=newQofPoint[1];
        rotate_point.y=newQofPoint[2];
        rotate_point.z=newQofPoint[3];
    }

    private double[] multQuat(double[] q1,double[] q2){
        double qw=q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2]-q1[3]*q2[3];
        double qx=q1[0]*q2[1]+q1[1]*q2[0]+q1[2]*q2[3]-q1[3]*q2[2];
        double qy=q1[0]*q2[2]+q1[2]*q2[0]-q1[1]*q2[3]+q1[3]*q2[1];
        double qz=q1[0]*q2[3]+q1[3]*q2[0]+q1[1]*q2[2]-q1[2]*q2[1];
        return new double[]{qw,qx,qy,qz};
    }
    private double[] revQuat(double[] q){
        return new double[]{q[0],-q[1],-q[2],-q[3]};
    }

    void rotateCube(Vector3D axis){
        for(Point3D point: p)
            rotate(axis,point);
    }

    Point3D[] returnDraw(){
        return new Point3D[]{       //12 ребер  рисовка по парам
                p[0],p[1],
                p[0],p[2],
                p[2],p[4],
                p[1],p[4],

                p[0],p[3],
                p[3],p[5],
                p[2],p[5],

                p[1],p[6],
                p[3],p[6],

                p[4],p[7],
                p[6],p[7],

                p[5],p[7]
        };
    }
}
