package by.geometry.sharubapv.lab12;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

public class Lab12 extends JComponent implements ActionListener {
    private Timer timer;
    private Color color = new Color(0, 0, 0);
    private Cube cube;
    private Point3D centralPoint;
    private Vector3D RV;

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    Lab12() {
        Vector3D v1 = new Vector3D(0, 4, 0);
        Vector3D v2 = new Vector3D(4, 0, 0);
        Point3D p = new Point3D(-100, -100, 400);
        cube = new Cube(p, v1, v2, 200);
        centralPoint= new Point3D(0, 0, -500);
        RV=new Vector3D(30,150,300);
        //RV=new Vector3D(p);
        timer = new Timer(5, this);
        setPreferredSize(new Dimension(1000, 1000));
    }

    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    private double findProection(Point3D p, boolean x) {
        if (x)
            return (p.x * centralPoint.z - p.z * centralPoint.x) / (centralPoint.z - p.z);
        else
            return (p.y * centralPoint.z - p.z * centralPoint.y) / (centralPoint.z - p.z);
    }



    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        Graphics2D g2 = (Graphics2D) g;
        int c = 300;
        g2.setColor(color);
        Point3D[] cubeP = cube.returnDraw();
        for (int i = 0; i < cubeP.length - 1; i += 2) {
            double x1 = findProection(cubeP[i], true);
            double y1 = findProection(cubeP[i], false);
            double x2 = findProection(cubeP[i + 1], true);
            double y2 = findProection(cubeP[i + 1], false);
            g2.draw(new Line2D.Double(c + x1, c - y1, c + x2, c - y2));
        }
        c=600;
        for (int i = 0; i < cubeP.length - 1; i += 2) {
            g2.draw(new Line2D.Double(c + cubeP[i].x, c - cubeP[i].y, c + cubeP[i+1].x, c - cubeP[i+1].y));
        }
        cube.rotateCube(RV);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                JFrame frame = new JFrame("Lab 12");
                JPanel panel = new JPanel();
                final Lab12 g = new Lab12();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(1100, 1000);
                frame.setVisible(true);
            }
        });
    }
}
