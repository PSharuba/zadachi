package by.geometry.sharubapv.lab12;

public class Vector3D {
    public double x;
    public double y;
    public double z;

    public Vector3D() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D(Point3D p1,Point3D p2) {
        this.x=p2.x-p1.x;
        this.y=p2.y-p1.y;
        this.z=p2.z-p1.z;
    }

    public Vector3D(Point3D p){
        this.x=p.x;
        this.y=p.y;
        this.z=p.z;
    }

    public void setNew(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double length(){
        return Math.sqrt(x*x+y*y+z*z);
    }

    public void norm(){
        double length=length();
        x/=length;
        y/=length;
        z/=length;
    }

    public Vector3D vectProd(Vector3D v){
        double x=this.y*v.z-this.z*v.y;
        double y=this.z*v.x-this.x*v.z;
        double z=this.x*v.y-this.y*v.x;
        return new Vector3D(x,y,z);
    }

    public Vector3D mult(int len){
        return new Vector3D(x*len,y*len,z*len);
    }
}
