package by.geometry.sharubapv.Functions;


import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class Drawing extends JComponent {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.clearRect(-10, -10, 2000, 2000);
        int c = 500;
        g.setColor(Color.black);
        g.drawLine(0, c, 2 * c, c);
        g.drawLine(c, 0, c, 2 * c);
        g.drawLine(2 * c, c, 2 * c - 5, c - 5);
        g.drawLine(2 * c, c, 2 * c - 5, c + 5);
        g.drawLine(c, 0, c - 5, 5);
        g.drawLine(c, 0, c + 5, 5);
        //End of axis
        g.setColor(Color.RED);
        for (int i = 0; i < polynom.size() - 1; i++) {
            g.drawLine(polynom.get(i).x + c, c - polynom.get(i).y, c + polynom.get(i + 1).x, c - polynom.get(i + 1).y);
        }
        for (int i = 0; i < polarPoints.size() - 1; i++) {
            g.drawLine(polarPoints.get(i).x + c, c - polarPoints.get(i).y, c + polarPoints.get(i + 1).x, c - polarPoints.get(i + 1).y);
        }
        g.setColor(Color.GREEN);
        for (int i = 0; i < points.size(); i++) {
            g.fillOval(points.get(i).x + c - 5, c - points.get(i).y - 5, 10, 10);
        }
        g.setColor(Color.MAGENTA);
        for (int i = 0; i < oddPoints.size() - 1; i += 2) {
            g.drawLine(oddPoints.get(i).x + c, c - oddPoints.get(i).y, c + oddPoints.get(i + 1).x, c - oddPoints.get(i + 1).y);
        }

    }
    private final LinkedList<Point> polynom = new LinkedList<>();
    private final LinkedList<Point> points = new LinkedList<>();
    private final LinkedList<PointWithAngle> polarPoints = new LinkedList<>();
    private final LinkedList<PointWithAngle> oddPoints = new LinkedList<>();

    public void addPoint2Polynom(int x, int y) {
        polynom.add(new Point(x, y));
        repaint();
    }



    public void addPolarPoint2Polynom(PointWithAngle p) {
        polarPoints.add(p);
        repaint();
    }

    public void addPoint(int x, int y) {
        points.add(new Point(x, y));
        repaint();
    }

    public void clear() {
        polynom.clear();
        polarPoints.clear();
        points.clear();
        repaint();
    }



    public void removeLastPoint() {
        oddPoints.add(polarPoints.get(polarPoints.size() - 2));
        oddPoints.add(polarPoints.get(polarPoints.size() - 1));
        polarPoints.removeLast();
        repaint();
    }
}
