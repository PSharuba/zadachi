package by.geometry.sharubapv.Functions;

public class PointWithAngle extends Point implements Comparable<PointWithAngle>{
    public double cos;
    public PointWithAngle(Point p,double cos){
        super(p.x,p.y);
        this.cos=cos;
    }
    public int compareTo(PointWithAngle o){
        if(this.cos>o.cos) return 1;
        if(this.cos<o.cos) return -1;
        return 0;
    }
    @Override
    public String toString(){
        return "( "+x+" ; "+y+" ), cos= "+cos+"\n";
    }
}
