package by.geometry.sharubapv.lab5;

import by.geometry.sharubapv.Functions.Functions;
import by.geometry.sharubapv.lab3.DoublePoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Lab5Mov extends JComponent implements ActionListener {
    private Timer timer;

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    Lab5Mov() {
        timer = new Timer(10, this);
        setPreferredSize(new Dimension(1000, 1000));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    public static ArrayList<DoublePoint> points = new ArrayList<>();
    public static ArrayList<DoublePoint> CH = new ArrayList<>();
    public static ArrayList<DoublePoint> diam=new ArrayList<>();

    public static DoublePoint findMaxXY(ArrayList<DoublePoint> arr) {
        double ymax = arr.get(0).y;
        int j = 0;
        ArrayList<DoublePoint> minPoints = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).y > ymax) {
                ymax = arr.get(i).y;
                j = i;
            }
        }
        return arr.get(j);
    }

    public static void Jarvise() {
        DoublePoint s0 = findMaxXY(points);
        CH.add(s0);
        DoublePoint next;
        DoublePoint curr;
        DoublePoint last;
        while (true) { //UP
            int len = points.size();
            last = CH.get(CH.size() - 1);
            int j = 0;
            while (j < len) {
                if (points.get(j).y < last.y) break;
                j++;
            }
            if (j < len) {
                curr = points.get(j);
            } else break;
            for (int i = j + 1; i < len; i++) {
                next = points.get(i);
                if (next.y < last.y) {
                    if (Functions.isLeft(next, new DoublePoint[]{curr, last}) > 0)
                        curr = next;
                }
            }
            CH.add(curr);
        }
        while (true) {// DOWN
            int len = points.size();
            last = CH.get(CH.size() - 1);
            int j = 0;
            while (j < len) {
                if (points.get(j).y > last.y) break;
                j++;
            }
            if (j < len) {
                curr = points.get(j);
            } else break;
            for (int i = j + 1; i < len; i++) {
                next = points.get(i);
                if (next.y > last.y) {
                    if (Functions.isLeft(next, new DoublePoint[]{curr, last}) > 0)
                        curr = next;
                }
            }
            CH.add(curr);
        }
    }

    private static double square(DoublePoint a,DoublePoint b,DoublePoint c){
        double[] v1=new double[]{b.x-a.x,b.y-a.y};
        double[] v2=new double[]{c.x-a.x,c.y-a.y};
            return v1[0] * v2[0] + v1[1] * v2[1];

    }
    private static double distance(DoublePoint a, DoublePoint b){
        double[] v=new double[]{a.x-b.x,a.y-b.y};
        return Math.sqrt(v[0]*v[0]+v[1]*v[1]);
    }
    private void turnAround(double[] indexes){
        if(indexes[0]>500){
            CH.get((int)indexes[1]).angle+=Math.PI;
            //CH.get((int)indexes[1]).x+=Math.cos(CH.get((int)indexes[1]).angle)*20;
            //CH.get((int)indexes[1]).y+=Math.sin(CH.get((int)indexes[1]).angle)*20;
            CH.get((int)indexes[2]).angle+=Math.PI;
            //CH.get((int)indexes[2]).x+=Math.cos(CH.get((int)indexes[2]).angle)*20;
            //CH.get((int)indexes[2]).y+=Math.sin(CH.get((int)indexes[2]).angle)*20;
        }
    }
    private void border(DoublePoint p){
        if(Math.abs(p.x)>450 || Math.abs(p.y)>450)
            p.angle+=Math.PI;
    }

    public static double[] diametr(){
        DoublePoint[] arr=CH.toArray(new DoublePoint[0]);
        int len =arr.length;
        int s=0;
        int e=0;
        double d=0;
        int i=1;
        int j=0;
        double[] res = new double[]{0,0,0};
        while (square(arr[len-1],arr[0],arr[i])<square(arr[len-1],arr[0],arr[i+1]) && i<len-2)
            i++;
        s=i;
        while (j<len-1) {
            i=s;
            while (square(arr[j],arr[j+1],arr[i])<=square(arr[j],arr[j+1],arr[i+1]) && i<len-2)
                i++;
            e=i;
            for (int k=s;k<=e;k++){
                d=distance(arr[j],arr[k]);
                if(d>res[0]) {
                    res[0] = d;
                    res[1]=j;
                    res[2]=k;
                }
            }
            j=j+1;
            s=e;
        }
        diam.clear();
        diam.add(CH.get((int)res[1]));
        diam.add(CH.get((int)res[2]));
        return res;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        int c = 500;
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        Jarvise();
        //diametr();
        double[] indexes=diametr();
        turnAround(indexes);
        for (DoublePoint p : points)
            g.fillOval((int) p.x + c - 5, c - (int) p.y - 5, 10, 10);
        for (int i = 0; i < CH.size() - 1; i++)
            g.drawLine(c + (int) CH.get(i).x, c - (int) CH.get(i).y,
                    c + (int) CH.get(i + 1).x, c - (int) CH.get(i + 1).y);
        g.setColor(Color.GREEN);
        g.drawLine((int)diam.get(0).x+c,c-(int)diam.get(0).y,
                (int)diam.get(1).x+c,c-(int)diam.get(1).y);
        CH.clear();
        diam.clear();
        for(int i=0;i<points.size();i++){
            border(points.get(i));
            points.get(i).x+=Math.cos(points.get(i).angle);
            points.get(i).y+=Math.sin(points.get(i).angle);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (int i = 0; i < 20; i++)
                    points.add(new DoublePoint((int) (Math.random() * 400 - 200), (int) (Math.random() * 400 - 200)));
                JFrame frame = new JFrame("Lab 5");
                JPanel panel = new JPanel();
                final Lab5Mov g = new Lab5Mov();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(1000, 1000);
                frame.setVisible(true);
            }
        });
    }
}