package by.geometry.sharubapv.lab8;

import by.geometry.sharubapv.lab3.DoublePoint;
import by.geometry.sharubapv.lab3.Functions;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Comparator;

public class Lab8 extends JFrame {
    private DrawingPane pane;
    private ArrayList<DoublePoint> polygon;
    private ArrayList<DoublePoint> line;
    private ArrayList<DoublePoint> lineSegment;

    public Lab8() {
        polygon = new ArrayList<>();
        generateConvexPoly();
        line = new ArrayList<>();
        lineSegment = new ArrayList<>();
        setTitle("PointDraw");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pane = new DrawingPane();
        JPanel topPanel = new JPanel();
        JButton clearButton = new JButton("Clear");
        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                line.clear();
                lineSegment.clear();
                pane.repaint();
            }
        });
        topPanel.add(clearButton);

        add(pane);
        add(topPanel, BorderLayout.NORTH);
        pack();
    }

    private void generateConvexPoly(){
        for(int i=0;i<20;i++){
            polygon.add(new DoublePoint(500,500));
            polygon.get(polygon.size()-1).x+=Math.cos(polygon.get(polygon.size()-1).angle)*300;
            polygon.get(polygon.size()-1).y+=Math.sin(polygon.get(polygon.size()-1).angle)*300;
        }
        polygon.sort(Comparator.naturalOrder());
        polygon.add(polygon.get(0));
    }

    private double scalar(double[] v1, double[] v2) {
        return v1[0] * v2[0] + v1[1] * v2[1];
    }

    private DoublePoint intersectionPoint(DoublePoint p11, DoublePoint p12, DoublePoint p21, DoublePoint p22) {
        double a1 = p11.y - p12.y;
        double b1 = p12.x - p11.x;
        double a2 = p21.y - p22.y;
        double b2 = p22.x - p21.x;
        double c1 = p12.y * p11.x - p12.x * p11.y;
        double c2 = p22.y * p21.x - p22.x * p21.y;
        double d = a1 * b2 - a2 * b1;
        double x = (b1 * c2 - b2 * c1) / d;
        double y = (a2 * c1 - a1 * c2) / d;
        return new DoublePoint(x, y);
    }

    private double parameter(DoublePoint p11, DoublePoint p12, DoublePoint p21, DoublePoint p22) { //1 прямая - АВ      2-сторона мн-ка
        DoublePoint p = intersectionPoint(p11, p12, p21, p22);//точка перечечения
        double[] v1 = new double[]{p.x - p11.x, p.y - p11.y};
        double[] v2 = new double[]{p12.x - p11.x, p12.y - p11.y};
        double len1 = Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
        double len2 = Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
        double param = len1 / len2;
        if (scalar(v1, v2) > 0) return param;
        else return -param;
    }

    private double parameterOfPoint(DoublePoint p1, DoublePoint p2, DoublePoint p0) {
        double[] v1 = new double[]{p0.x - p1.x, p0.y - p1.y};
        double[] v2 = new double[]{p2.x - p1.x, p2.y - p1.y};
        double len1 = Math.sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
        double len2 = Math.sqrt(v2[0] * v2[0] + v2[1] * v2[1]);
        double param = len1 / len2;
        if (scalar(v1, v2) > 0) return param;
        else return -param;
    }


    private int type(DoublePoint p1, DoublePoint p2) {
        double[] n = new double[]{p2.y - p1.y, p1.x - p2.x};
        double[] AB = new double[]{line.get(1).x - line.get(0).x, line.get(1).y - line.get(0).y};
        double s = scalar(n, AB);
        if (s < 0) return 2;           //2 - вход
        if (s > 0) return 1;           //1 - выход
        else return 0;              //0 - на одной прямой
    }

    private DoublePoint pointByParam(double p) {
        double[] AB = new double[]{line.get(1).x - line.get(0).x, line.get(1).y - line.get(0).y};
        return new DoublePoint(line.get(0).x + AB[0] * p, line.get(0).y + AB[1] * p);
    }

    private void lineSegment() {
        lineSegment.clear();
        double[] T = new double[]{0., 1.};
        for (int i = 0; i < polygon.size() - 1; i++) {
            if (lineIntersections(new DoublePoint[]{polygon.get(i), polygon.get(i + 1)},
                    new DoublePoint[]{line.get(0), line.get(1)})) {
                double t = parameter(line.get(0), line.get(1), polygon.get(i), polygon.get(i + 1));
                int type = type(polygon.get(i), polygon.get(i + 1));
                if (type == 2) T[0] = Math.max(T[0], t);
                if (type == 1) T[1] = Math.min(T[1], t);
                if (type == 0) {
                    double pA=parameterOfPoint(polygon.get(i), polygon.get(i + 1),line.get(0));
                    double pB=parameterOfPoint(polygon.get(i), polygon.get(i + 1),line.get(1));
                    if(pA>=0 && pA<=1) lineSegment.add(line.get(0));
                    if(pB>=0 && pB<=1) lineSegment.add(line.get(1));
                    if(lineSegment.size()==2) return;
                    if(lineSegment.size()==0){
                        lineSegment.add(polygon.get(i));
                        lineSegment.add(polygon.get(i+1));
                        return;
                    }
                    if(pA>=0 && pA<=1){
                        if(pB>1) lineSegment.add(polygon.get(i+1));
                        else lineSegment.add(polygon.get(i));
                        return;
                    }
                    if(pB>=0 && pB<=1){
                        if(pA>1) lineSegment.add(polygon.get(i+1));
                        else lineSegment.add(polygon.get(i));
                        return;
                    }
                }
            }
        }
        if (T[0] == T[1])
            lineSegment.add(pointByParam(T[0]));
        if (T[0] < T[1]) {
            lineSegment.add(pointByParam(T[0]));
            lineSegment.add(pointByParam(T[1]));
        }
        if(T[0]==0 && T[1]==1 && !Functions.octanTest(line.get(0),polygon))
            lineSegment.clear();
    }

    private boolean lineIntersections(DoublePoint[] line1, DoublePoint[] line2) {
        double[] v13 = Functions.substract(line2[0], line1[0]);
        double[] v31 = Functions.substract(line1[0], line2[0]);
        double[] v23 = Functions.substract(line2[0], line1[1]);
        double[] v32 = Functions.substract(line1[1], line2[0]);
        double[] v14 = Functions.substract(line2[1], line1[0]);
        double[] v41 = Functions.substract(line1[0], line2[1]);
        double[] v24 = Functions.substract(line2[1], line1[1]);
        double[] v42 = Functions.substract(line1[1], line2[1]);
        double ang1 = v13[0] * v14[0] + v13[1] * v14[1];
        double ang2 = v23[0] * v24[0] + v23[1] * v24[1];
        double ang3 = v31[0] * v32[0] + v31[1] * v32[1];
        double ang4 = v41[0] * v42[0] + v41[1] * v42[1];
        double det1 = Functions.isLeft(line2[0], line1);
        double det2 = Functions.isLeft(line2[1], line1);
        double det3 = Functions.isLeft(line1[0], line2);
        double det4 = Functions.isLeft(line1[1], line2);
        if (det1 == 0 && det2 == 0 && det3 == 0 && det4 == 0) {
            return (ang1 <= 0 || ang2 <= 0 || ang3 <= 0 || ang4 <= 0);
        }
        if ((det1 * det2) > 0 || (det3 * det4) > 0) {
            return false;
        }
        return ((det1 * det2) <= 0 && (det3 * det4) <= 0);
    }

    class DrawingPane extends JComponent {
        public DrawingPane() {
            setPreferredSize(new Dimension(1000, 1000));
            setBorder(new LineBorder(Color.GRAY));
            addMouseListener(new MouseAdapter() {
                                 @Override
                                 public void mouseClicked(MouseEvent e) {
                                     DoublePoint point = new DoublePoint(e.getX(), e.getY());
                                     if (line.size() == 2)
                                         line.clear();
                                     line.add(point);
                                     repaint();
                                 }
                             }
            );
        }

        @Override
        protected void paintComponent(Graphics g) {
            g.clearRect(-100, -100, 2000, 2000);
            super.paintComponent(g);
            g.setColor(Color.BLACK);
            for (int i = 0; i < polygon.size() - 1; i++)
                g.drawLine((int) polygon.get(i).x, (int) polygon.get(i).y, (int) polygon.get(i + 1).x, (int) polygon.get(i + 1).y);
            if (line.size() == 2) {
                g.setColor(Color.BLUE);
                g.drawLine((int) line.get(0).x, (int) line.get(0).y, (int) line.get(1).x, (int) line.get(1).y);
                g.setColor(Color.red);
                lineSegment();
                if(lineSegment.size()==0)
                    g.drawString("No intersection",400,400);
                if(lineSegment.size()==1)
                    g.fillOval((int)lineSegment.get(0).x-5,(int)lineSegment.get(0).y-5,10,10);
                if(lineSegment.size()==2) {
                    g.fillOval((int)lineSegment.get(0).x-5,(int)lineSegment.get(0).y-5,10,10);
                    g.fillOval((int)lineSegment.get(1).x-5,(int)lineSegment.get(1).y-5,10,10);
                    g.drawLine((int) lineSegment.get(0).x, (int) lineSegment.get(0).y, (int) lineSegment.get(1).x, (int) lineSegment.get(1).y);
                    g.drawLine((int) lineSegment.get(0).x, (int) lineSegment.get(0).y+1, (int) lineSegment.get(1).x, (int) lineSegment.get(1).y+1);
                    g.drawLine((int) lineSegment.get(0).x, (int) lineSegment.get(0).y-1, (int) lineSegment.get(1).x, (int) lineSegment.get(1).y-1);
                }
            }
        }
    }

    public static void main(String[] args) {
        new Lab8().setVisible(true);
    }
}
