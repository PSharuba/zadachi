package by.geometry.sharubapv.lab6;

import by.geometry.sharubapv.lab3.DoublePoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Lab6 extends JComponent implements ActionListener {
    private Timer timer;

    public void start() {
        timer.start();
    }

    public void stop() {
        timer.stop();
    }

    Lab6() {
        timer = new Timer(10, this);
        setPreferredSize(new Dimension(1000, 1000));
    }

    public void actionPerformed(ActionEvent arg0) {
        repaint();
    }

    public static ArrayList<DoublePoint> points = new ArrayList<>();
    static ArrayList<DoublePoint> CH = new ArrayList<>();
    static DoublePoint L;
    static DoublePoint R;


    double pointLocation(DoublePoint A, DoublePoint B, DoublePoint P) {
        return  (B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x);
    }

    double distance(DoublePoint a, DoublePoint b, DoublePoint c) {
        double[] v1 = new double[]{a.x - b.x, a.y - b.y};
        double[] v2 = new double[]{c.x - b.x, c.y - b.y};
        return Math.abs(v1[0] * v2[1] - v1[1] * v2[0]);
    }

    void quickHull() {
        //ArrayList<DoublePoint> points = (ArrayList) Points.clone();
        ArrayList<DoublePoint> convexHull = new ArrayList<>();

        int minPoint = -1, maxPoint = -1;
        double minX = Double.MAX_VALUE;
        double maxX = Double.MIN_VALUE;
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).x < minX) {
                minX = points.get(i).x;
                minPoint = i;
            }
            if (points.get(i).x > maxX) {
                maxX = points.get(i).x;
                maxPoint = i;
            }
        }

        DoublePoint A = points.get(minPoint); //Left point
        DoublePoint B = points.get(maxPoint); //Right point
        L = A;
        R = B;
        convexHull.add(A);
        convexHull.add(B);

        ArrayList<DoublePoint> leftSet = new ArrayList<>(); //left LR
        ArrayList<DoublePoint> rightSet = new ArrayList<>();//right LR

        for (int i = 0; i < points.size(); i++) {
            DoublePoint p = points.get(i);
            if (pointLocation(A, B, p) <0)
                leftSet.add(p);
            if(pointLocation(A, B, p) >0)
                rightSet.add(p);
        }

        hullSet(A, B, rightSet, convexHull); //Сборка левого мн-ва в оболочку
        hullSet(B, A, leftSet, convexHull);
        convexHull.add(convexHull.get(0));
        CH = convexHull;
    }

    void hullSet(DoublePoint A, DoublePoint B, ArrayList<DoublePoint> set, ArrayList<DoublePoint> hull) {
        int insertPosition = hull.indexOf(B);
        if (set.size() == 0) return;
        if (set.size() == 1) {
            DoublePoint p = set.get(0);
            set.remove(p);
            hull.add(insertPosition, p);
            return;
        }

        double dist = Double.MIN_VALUE;
        int furthestPoint = -1;
        for (int i = 0; i < set.size(); i++) {
            DoublePoint p = set.get(i);
            double distance = distance(A, B, p);
            if (distance > dist) {
                dist = distance;
                furthestPoint = i;
            }
        }

        DoublePoint P = set.get(furthestPoint);
        set.remove(furthestPoint);
        hull.add(insertPosition, P);

        ArrayList<DoublePoint> leftSetAP = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            DoublePoint M = set.get(i);
            if (pointLocation(A, P, M) > 0) {
                leftSetAP.add(M);
            }
        }

        ArrayList<DoublePoint> leftSetPB = new ArrayList<>();
        for (int i = 0; i < set.size(); i++) {
            DoublePoint M = set.get(i);
            if (pointLocation(P, B, M) > 0) {
                leftSetPB.add(M);
            }
        }

        hullSet(A, P, leftSetAP, hull);
        hullSet(P, B, leftSetPB, hull);
    }

    double length(DoublePoint a,DoublePoint b){
        double[] v=new double[]{a.x-b.x,a.y-b.y};
        return Math.sqrt(v[0]*v[0]+v[1]*v[1]);
    }

    double perimeter(){
        double p=0;
        for(int i=0;i<CH.size()-1;i++){
            p+=length(CH.get(i),CH.get(i+1));
        }
        return p;
    }
    private void turnAround() {
        if (perimeter() > 2000) {
            for (DoublePoint p : CH) {
                p.angle += Math.PI;
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(-10, -10, 2000, 2000);
        int c = 500;
        CH.clear();
        quickHull();
        g.setColor(Color.GREEN);
        g.fillOval((int) L.x + c - 10, c - (int) L.y - 10, 20, 20);
        g.fillOval((int) R.x + c - 10, c - (int) R.y - 10, 20, 20);
        g.setColor(Color.BLACK);
        for (DoublePoint p : points)
            g.fillOval((int) p.x + c - 5, c - (int) p.y - 5, 10, 10);
        for (int i = 0; i < CH.size() - 1; i++)
            g.drawLine(c + (int) CH.get(i).x, c - (int) CH.get(i).y,
                    c + (int) CH.get(i + 1).x, c - (int) CH.get(i + 1).y);
        turnAround();
        for(int i=0;i<points.size();i++){
            points.get(i).x+=Math.cos(points.get(i).angle);
            points.get(i).y+=Math.sin(points.get(i).angle);
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                for (int i = 0; i < 20; i++)
                    points.add(new DoublePoint((int) (Math.random() * 400 - 200), (int) (Math.random() * 400 - 200)));
                JFrame frame = new JFrame("Lab 6");
                JPanel panel = new JPanel();
                final Lab6 g = new Lab6();
                panel.add(g);
                frame.getContentPane().add(panel);
                final JButton button = new JButton("Start");
                button.addActionListener(new ActionListener() {
                    private boolean pulsing = false;

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (pulsing) {
                            pulsing = false;
                            g.stop();
                            button.setText("Start");
                        } else {
                            pulsing = true;
                            g.start();
                            button.setText("Stop");
                        }
                    }
                });
                panel.add(button);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setSize(1000, 1000);
                frame.setVisible(true);
            }
        });
    }
}
