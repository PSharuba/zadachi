package by.geometry.sharubapv.lab7;

import by.geometry.sharubapv.lab3.DoublePoint;
import by.geometry.sharubapv.lab3.Functions;


import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Lab7 extends JFrame {
    private ArrayList<DoublePoint> points;
    private DrawingPane pane;
    private boolean insidePane = false;
    private ArrayList<DoublePoint> CH;

    private void buildHull() {
        if (points.size() == 1) {
            CH = (ArrayList) points.clone();
        }
        if (points.size() == 2)
            if (!points.get(0).equals(points.get(1))) {
                CH = (ArrayList) points.clone();
            } else {
                points.remove(1);
            }
        if (points.size() == 3) {
            for (DoublePoint p : CH) {
                if (p.equals(points.get(2)))
                    return;
            }
            if (Functions.isLeft(points.get(2), new DoublePoint[]{CH.get(0), CH.get(1)}) > 0) {
                CH.add(points.get(2));
                CH.add(CH.get(0));
            } else if (Functions.isLeft(points.get(2), new DoublePoint[]{CH.get(0), CH.get(1)}) < 0) {
                CH.clear();
                CH.add(points.get(0));
                CH.add(points.get(2));
                CH.add(points.get(1));
                CH.add(points.get(0));
            }
        }
        if (points.size() > 3) {
            ArrayList<DoublePoint> odd = new ArrayList<>();
            int s = -1;
            for (int i = 0; i < CH.size() - 1; i++) {
                if (Functions.isLeft(points.get(points.size() - 1), new DoublePoint[]{CH.get(i), CH.get(i + 1)}) < 0) {
                    odd.add(CH.get(i));
                    if (s == -1)
                        s = i;
                }
            }
            if (s == 0 && odd.get(odd.size()-1).equals(CH.get(CH.size()-2))) {
                int i = CH.size() - 1;
                while (i > 0 && Functions.isLeft(points.get(points.size() - 1), new DoublePoint[]{CH.get(i - 1), CH.get(i)}) < 0)
                    i--;
                odd.remove(CH.get(i));
                CH.remove(CH.size() - 1);
                CH.add(points.get(points.size() - 1));
                CH.add(0,points.get(points.size() - 1));
            } else {
                if (odd.size() == 1) {
                    CH.add(CH.indexOf(odd.get(0)) + 1, points.get(points.size() - 1));
                    odd.remove(0);
                }
                if (odd.size() > 1) {
                    odd.remove(0);
                    CH.add(CH.indexOf(odd.get(0)) + 1, points.get(points.size() - 1));
                }
            }
            for (DoublePoint p : odd) {
                CH.remove(p);
            }
        }
    }

    public Lab7() {
        setTitle("PointDraw");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        points = new ArrayList<>();
        CH = new ArrayList<>();
        pane = new DrawingPane();

        JPanel topPanel = new JPanel();

        JButton clearButton = new JButton("Clear");
        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                points.clear();
                CH.clear();
                pane.repaint();
            }
        });
        topPanel.add(clearButton);

        add(pane);
        add(topPanel, BorderLayout.NORTH);
        pack();
    }

    class DrawingPane extends JComponent {
        public DrawingPane() {
            setPreferredSize(new Dimension(600, 400));
            setBorder(new LineBorder(Color.GRAY));
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (insidePane) {
                        DoublePoint point = new DoublePoint(e.getX(), e.getY());
                        points.add(point);
                        repaint();
                    }
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    insidePane = true;
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    insidePane = false;
                }
            });
        }

        @Override
        protected void paintComponent(Graphics g) {
            g.clearRect(-100, -100, 2000, 2000);
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            for (DoublePoint p : points) {
                g2.setColor(Color.BLACK);
                int x = (int) p.x;
                int y = (int) p.y;
                int size = 10;
                g2.fillOval(x - size / 2, y - size / 2, size, size);
            }
            buildHull();
            g.setColor(Color.BLUE);
            for (int i = 0; i < CH.size() - 1; i++)
                g2.drawLine((int) CH.get(i).x, (int) CH.get(i).y, (int) CH.get(i + 1).x, (int) CH.get(i + 1).y);
        }
    }

    public static void main(String[] args) {
        new Lab7().setVisible(true);
    }
}
