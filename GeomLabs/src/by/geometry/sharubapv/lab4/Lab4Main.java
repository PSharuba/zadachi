package by.geometry.sharubapv.lab4;

import by.geometry.sharubapv.Functions.*;
import by.geometry.sharubapv.Functions.Point;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Lab4Main {
    public static void main(String[] args) {
        Drawing g = new Drawing();
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        g.setPreferredSize(new Dimension(1000, 1000));
        mainFrame.getContentPane().add(g, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setVisible(true);
        ArrayList<Point> pointsArr = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            int x = (int) (Math.random() * 1000 - 500);
            int y = (int) (Math.random() * 1000 - 500);
            pointsArr.add(new by.geometry.sharubapv.Functions.Point(x, y));
            g.addPoint(x, y);
        }
        int iMin = Functions.findMinXY(pointsArr);
        by.geometry.sharubapv.Functions.Point pMin = pointsArr.get(iMin);
        ArrayList<PointWithAngle> points = new ArrayList<>();
        for (by.geometry.sharubapv.Functions.Point p : pointsArr) {
            if (p != pMin)
                points.add(new PointWithAngle(p, Functions.polarAngle(pMin, p, true)));
        }
        Collections.sort(points, Comparator.reverseOrder());
        ArrayList<PointWithAngle> CH = new ArrayList<>();
        CH.add(new PointWithAngle(pMin, 0));
        CH.add(points.get(0));
        g.addPolarPoint2Polynom(new PointWithAngle(pMin, 0));
        g.addPolarPoint2Polynom(points.get(0));
        for (int i = 1; i < points.size(); i++) {
            if (Functions.isLeft(points.get(i), new PointWithAngle[]{CH.get(CH.size() - 2), CH.get(CH.size() - 1)}) > 0) {
                g.addPolarPoint2Polynom(points.get(i));
                CH.add(points.get(i));
            } else {
                while (Functions.isLeft(points.get(i), new PointWithAngle[]{CH.get(CH.size() - 2), CH.get(CH.size() - 1)}) <= 0) {
                    CH.remove(CH.size() - 1);
                    g.removeLastPoint();
                }
                CH.add(points.get(i));
                g.addPolarPoint2Polynom(points.get(i));
            }
        }
        CH.add(CH.get(0));
        g.addPolarPoint2Polynom(CH.get(0));
    }
}
