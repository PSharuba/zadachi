package by.geometry.sharubapv.lab2;

import by.geometry.sharubapv.Functions.*;
import by.geometry.sharubapv.Functions.Point;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Lab2Main {
    public static void main(String[] args) {
        Drawing g = new Drawing();
        JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        g.setPreferredSize(new Dimension(1000, 1000));
        mainFrame.getContentPane().add(g, BorderLayout.CENTER);
        mainFrame.pack();
        mainFrame.setVisible(true);
        int koef = 50;
        by.geometry.sharubapv.Functions.Point p0 = new by.geometry.sharubapv.Functions.Point(-5, -1);
        ArrayList<by.geometry.sharubapv.Functions.Point> polynom = new ArrayList<>();
        polynom.add(new by.geometry.sharubapv.Functions.Point(0, 3));
        polynom.add(new by.geometry.sharubapv.Functions.Point(2, 0));
        polynom.add(new by.geometry.sharubapv.Functions.Point(4, 3));
        polynom.add(new by.geometry.sharubapv.Functions.Point(8, 0));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-4, -7));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-8, -1));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-7, 2));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-7, 0));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-6, 0));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-5, -3));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-4, 0));
        polynom.add(new by.geometry.sharubapv.Functions.Point(-3, 0));
        polynom.add(new Point(-2, -2));
        polynom.add(new by.geometry.sharubapv.Functions.Point(0, 0));
        for (by.geometry.sharubapv.Functions.Point p : polynom)
            g.addPoint2Polynom(p.x * koef, p.y * koef);
        g.addPoint2Polynom(polynom.get(0).x * koef, polynom.get(0).y * koef);
        g.addPoint(p0.x * koef, p0.y * koef);
        if (Functions.pointInside(p0, polynom))
            System.out.println("Point inside");
        else System.out.println("Point outside");


    }
}