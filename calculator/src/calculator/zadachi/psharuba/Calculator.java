package calculator.zadachi.psharuba;

import java.util.Scanner;

public class Calculator {
    static Scanner scanner = new Scanner(System.in);

    public double a,b;
    public char sign;
    public  static  double getNum(){
        System.out.println("Enter number:");
        double number;
        if(scanner.hasNextDouble()){
            number = scanner.nextDouble();
        } else {
            System.out.println("Error in number. Try again.");
            number = getNum();
        }
        return number;
    }
    public static char getOperation(){
        System.out.println("Enter operation:");
        char operation;
        if(scanner.hasNext()){
            operation = scanner.next().charAt(0);
            if(!(operation == 43 || operation == 42 || operation == 45 || operation == 47)){
                System.out.println("Wrong operation. Try again.");
                operation = getOperation();
            }
        } else {
            System.out.println("Error in operation. Try again.");
            operation = getOperation();
        }
        return operation;
    }
    public String toString(){
        double res=0;
        switch (sign){
            case '+':
                res=a+b;
                break;
            case '-':
                res=a-b;
                break;
            case '*':
                res = a*b;
                break;
            case '/':
                res = a/b;
                break;
        }
        return (a + " " + sign + " " + b + " = " + res + '\n');
    }
}
