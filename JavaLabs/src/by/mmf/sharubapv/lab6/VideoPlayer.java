package by.mmf.sharubapv.lab6;

import java.util.Scanner;

public class VideoPlayer extends Player implements Technics {
    private Scanner sc = new Scanner(System.in);
    private int inputs;
    private int outputs;

    VideoPlayer() {
        super();
        this.inputs = 0;
        this.outputs = 0;
    }

    VideoPlayer(String name, String producer, double model, int inputs, int outputs) {
        super(name, producer, model);
        this.inputs = inputs;
        this.outputs = outputs;
    }

    @Override
    public void work() {
        if (this.power) {
            System.out.println("Choose disk drive: ");
            for (int i = 1; i <= inputs; i++)
                System.out.println("Drive №" + i + "\n");

            int drive = 0;
            while (drive < 1 || drive > inputs)
                drive = sc.nextInt();
            System.out.println("\nLoading from drive №" + drive);
            System.out.print("Loading ...\n" +
                    "* playing video ");
            if (outputs > 0)
                if (outputs > 1) System.out.print("1-" + outputs + " *\n");
                else System.out.print("1 output *\n");
            else System.out.print("*\n");
        } else System.out.println("\nNo power. Turn power on.\n");
    }

    public void set(String name, String producer, double model, int inputs, int outputs){
        this.name=name;
        this.producer=producer;
        this.model=model;
        this.inputs = inputs;
        this.outputs = outputs;
    }

    @Override
    public String toString() {
        String str = "Videoplayer " + name +
                "\nModel " + model +
                "\nInputs " + inputs +
                "\nOutputs " + outputs +
                "\nPower ";
        if (power) return str + "(ON)\n";
        else return str + "(OFF)\n";
    }
}
