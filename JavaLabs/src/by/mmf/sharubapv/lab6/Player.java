package by.mmf.sharubapv.lab6;

interface Technics {
    void turnOn();

    void turnOff();

    void work();
}

public abstract class Player implements Technics {
    String name;
    String producer;
    double model;
    boolean power;

    {
        power=false;
    }
    Player() {
    this.name="Player";
    this.producer="NaN";
    this.model=0;
    }
    Player(String name,String producer, double model) {
        this.name=name;
        this.producer=producer;
        this.model=model;
    }

    @Override
    public void turnOn() {
        this.power = true;
    }

    @Override
    public void turnOff() {
        this.power = false;
    }

    public void set(String name,String producer, double model){
        this.name=name;
        this.producer=producer;
        this.model=model;
    }
}
