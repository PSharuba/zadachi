package by.mmf.sharubapv.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Lab6Main {
    public static Scanner sc = new Scanner(System.in);

    public static void set(VideoPlayer p) {
        System.out.println("Adding new video player.\nEnter name and producer:");
        String name, producer;
        double model;
        int in, out;
        name = sc.nextLine();
        producer = sc.nextLine();
        System.out.println("\nEnter model number:");
        model = sc.nextDouble();
        System.out.println("\nEnter number of inputs and outputs:");
        in = sc.nextInt();
        out = sc.nextInt();
        sc.nextLine();
        p.set(name, producer, model, in, out);
    }

    public static void main(String[] args) {
        ArrayList<VideoPlayer> players = new ArrayList<>();
        players.add(new VideoPlayer());
        set(players.get(0));
        while (true) {
            System.out.println("1)Add new player\n" +
                    "2)Turn on player\n" +
                    "3)Turn off player\n" +
                    "4)Play video\n" +
                    "5)Show list of video players\n" +
                    "Any other key to Exit");
            char f = sc.nextLine().charAt(0);

            int i;
            switch (f) {
                case '1':
                    VideoPlayer p = new VideoPlayer();
                    set(p);
                    players.add(p);
                    break;
                case '2':
                    System.out.println("Which player you want to turn on? (1 - " + players.size() + ")");
                    i = sc.nextInt() - 1;
                    sc.nextLine();
                    if (i < 0 || i > players.size() - 1) {
                        System.out.println("Wrong player");
                        break;
                    }
                    players.get(i).turnOn();
                    break;
                case '3':
                    System.out.println("Which player you want to turn off? (1 - " + players.size() + ")");
                    i = sc.nextInt() - 1;
                    sc.nextLine();
                    if (i < 0 || i > players.size() - 1) {
                        System.out.println("Wrong player");
                        break;
                    }
                    players.get(i).turnOff();
                    break;
                case '4':
                    System.out.println("On which player you want to play video? (1 - " + players.size() + ")");
                    i = sc.nextInt() - 1;
                    sc.nextLine();
                    if (i < 0 || i > players.size() - 1) {
                        System.out.println("Wrong player");
                        break;
                    }
                    players.get(i).work();
                    break;
                case '5':
                    for (VideoPlayer pl : players) {
                        System.out.println(pl.toString());
                    }
                    break;
                default:
                    System.exit(0);
            }
        }
    }
}
