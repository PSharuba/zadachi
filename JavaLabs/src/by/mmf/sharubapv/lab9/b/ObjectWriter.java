package by.mmf.sharubapv.lab9.b;

import java.io.*;

public class ObjectWriter<E> {
    private String path;

    public ObjectWriter() {
        this.path = "./data/lab9/";
    }

    public ObjectWriter(String path) {
        this.path = path;
    }

    public void writeObject(String outputFileName, E object) throws IOException {
        FileOutputStream fos = new FileOutputStream(path + outputFileName + ".out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.flush();
        oos.close();
    }

    public E readObject(String outputFileName) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(path + outputFileName + ".out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        return (E) oin.readObject();
    }
}
