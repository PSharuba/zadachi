package by.mmf.sharubapv.lab9.a;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextSearch {
    private String path;
    private String name;

    public TextSearch() throws IOException {
        this.path = "./data/lab9/";
        this.name = "a";
    }

    public TextSearch(String path, String name) throws IOException {
        this.path = path;
        this.name = name;
        FileWriter fstream = new FileWriter(path + name + ".txt");
        System.out.println("Created new text file " + name + ".txt");
        fstream.close();
    }

    public String getContent() throws IOException {
        return new String(Files.readAllBytes(Paths.get(path + name + ".txt")), StandardCharsets.UTF_8);
    }

    public void search(String text) throws IOException {
        Pattern REGEX_WORD = Pattern.compile("[А-Яа-яA-Za-zЁё].+?(\\.|\\t|\\?|!|,|\\s|;|:)");
        ArrayList<String> words = new ArrayList<>();
        ArrayList<String> matches = new ArrayList<>();
        try {
            Matcher matcher = REGEX_WORD.matcher(text);
            while (matcher.find()) {
                words.add(text.substring(matcher.start(), matcher.end()));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        for (int i = 0; i < words.size() - 1; i++) {
            String w1 = words.get(i);
            String w2 = words.get(i + 1);
            if (!isLetter(w1)) w1 = w1.substring(0, w1.length() - 1);
            char c1 = Character.toLowerCase(w1.charAt(w1.length() - 1));
            char c2 = Character.toLowerCase(w2.charAt(0));
            if (c1 == c2) matches.add(w1);
        }
        FileWriter fstream = new FileWriter("./data/lab9/aMatches.txt");
        BufferedWriter out = new BufferedWriter(fstream);
        for (String w : matches) {
            out.write(w + '\n');
        }
        out.close();
        System.out.println("Saved matches to file aMatches.txt");
    }

    private boolean isLetter(String s) {
        int len = s.length();
        char lastC = s.charAt(len - 1);
        switch (lastC) {
            case ',':
            case '.':
            case ';':
            case ':':
            case '!':
            case '?':
            case '\"':
            case '\'':
            case '-':
                return false;
            default:
                return true;
        }
    }

    public static void main(String[] args) {
        try {
            TextSearch search = new TextSearch();
            search.search(search.getContent());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
