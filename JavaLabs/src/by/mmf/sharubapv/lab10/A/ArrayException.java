package by.mmf.sharubapv.lab10.A;

public class ArrayException extends Exception {
    public ArrayException(){ }
    public ArrayException(String message,Throwable cause){
        super(message,cause);
    }
    public ArrayException(String message){
        super(message);
    }
    public ArrayException(Throwable cause){
        super(cause);
    }
}
