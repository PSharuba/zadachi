package by.mmf.sharubapv.lab10.A;

import java.util.ArrayList;

public class Polynom {
    /*Умножить два многочлена заданной степени, если коэффициенты многочленов хранятся в различных списках.*/
    ArrayList<Double> koefficients;

    public Polynom() {
        koefficients = new ArrayList<>(1);
    }

    public Polynom(int degree) throws ArrayException {
        if (degree < 0) throw new ArrayException("Polynom's degree can't be less then 0");
        koefficients = new ArrayList<>(degree + 1);
    }

    public Polynom(ArrayList<Double> koefficients) {
        for (int i = koefficients.size() - 1; i >= 0; i--) {
            if (koefficients.get(i) == 0)
                koefficients.remove(koefficients.size() - 1);
            else break;
        }
        if(koefficients.size()==0) koefficients.add(0.);
        this.koefficients = koefficients;
    }

    public ArrayList<Double> getKoefficients() {
        return koefficients;
    }

    public double getElement(int i) {
        return koefficients.get(i);
    }


    public int getDegree() {
        return koefficients.size() - 1;
    }

    public Polynom product(Polynom p) {

        ArrayList<Double> newPolynom = new ArrayList<>(p.getDegree() + this.getDegree() + 1);
        for (int i = 0; i < p.getDegree() + this.getDegree() + 1; i++) {
            newPolynom.add((double) 0);
        }

        Double tmpCoef = (double) 0;
        for (int i = 0; i < this.getDegree() + 1; i++) {
            for (int j = 0; j < p.getDegree() + 1; j++) {
                tmpCoef = this.getElement(i) * p.getElement(j);
                tmpCoef += newPolynom.get(i + j);
                newPolynom.set(i + j, tmpCoef);
            }
        }
        return new Polynom(newPolynom);
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < koefficients.size(); i++) {
            if (koefficients.get(i) >= 0) result += "+";
            result += koefficients.get(i) + "*x^" + i + " ";
        }
        return result;
    }

    public static void main(String[] args) {
        ArrayList<Double> p1 = new ArrayList<>();
        ArrayList<Double> p2 = new ArrayList<>();
        {
            p1.add(1.0);
            p1.add(2.0);
            p1.add(3.0);
            p1.add(4.0);
            p1.add(0.);
            p1.add(0.);


            p2.add(1.0);
            p2.add(2.0);
            p2.add(3.0);
            p2.add(4.0);
            p2.add(5.0);
        }
        Polynom pol1 = new Polynom(p1);
        Polynom pol2 = new Polynom(p2);
        Polynom pol3;
        pol3 = pol1.product(pol2);
        System.out.println(pol1.toString() + "\n" + pol2.toString() + "\n\nResult:\n" + pol3.toString());
    }
}
