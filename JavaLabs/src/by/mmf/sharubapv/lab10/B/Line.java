package by.mmf.sharubapv.lab10.B;

public class Line {
     public Point a;
     public Point b;
     public Line(Point a,Point b){
         this.a=a;
         this.b=b;
     }

     @Override
     public String toString(){
         return (
                 "Line ("+ a.toString()+" , "+b.toString()+")"
                 );
     }
}
