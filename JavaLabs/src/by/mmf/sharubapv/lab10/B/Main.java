package by.mmf.sharubapv.lab10.B;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Point> points= new ArrayList<>();
        points.add(new Point(1,1));
        points.add(new Point(1,2));
        points.add(new Point(1,3));
        points.add(new Point(1,4));
        points.add(new Point(2,5));
        points.add(new Point(2,4));
        points.add(new Point(4,7));
        points.add(new Point(4,4));
        points.add(new Point(6,6));
        Plane plane=new Plane(points);
        try {
            plane.saveToFile("Lines.txt");
            System.out.println("saved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
