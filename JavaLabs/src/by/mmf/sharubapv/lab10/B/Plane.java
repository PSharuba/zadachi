package by.mmf.sharubapv.lab10.B;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Plane {   //12.
    // На плоскости задано N точек. Вывести в файл описания всех прямых,
    // которые проходят более чем через одну точку из заданных.
    // Для каждой прямой указать, через сколько точек она проходит.
    // Использовать класс HashMap.

    private List<Point> points;
    private HashMap<Integer, Point> pointMap;
    private HashMap<Integer, Line> lineMap;

    public Plane(int size) {
        try {
            if (size < 3) throw new IndexOutOfBoundsException("Size of plane can't be less than 3!");
        } catch (IndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
            size = 3;
        }
        for (int i = 0; i < size; ) {
            Point point = new Point(Math.random() * 10, Math.random() * 10);
            if (Arrays.stream(points.toArray()).noneMatch(point::equals)) {
                points.add(point);
                i++;
            }
        }
        setPointHashMap();
        setLineHashMap();
    }

    public int planeSize() {
        return points.size();
    }

    public List<Point> getPoints() {
        return points;
    }

    public Point getElement(int i) {
        return points.get(i);
    }

    public void setPoints(List<Point> points) {
        this.points = points;
        setPointHashMap();
        setLineHashMap();
    }

    public Plane(List<Point> points) {
        this.points = points;
        setPointHashMap();
        setLineHashMap();
    }

    private boolean intersection(Line line, Point p) {
        double a = line.a.y - line.b.y;
        double b = line.b.x - line.a.x;
        double c = line.b.y * line.a.x - line.b.x * line.a.y;
        double res = (a * p.x + b * p.y + c);
        return res == 0;
    }

    private void setLineHashMap() {
        HashMap<Integer, Line> lineMap = new HashMap();
        int lines = 0;
        for (int i = 0; i < planeSize(); i++) {
            for (int j = i + 1; j < planeSize(); j++) {
                lineMap.put(lines, new Line(getElement(i), getElement(j)));
                lines++;
            }
        }
        this.lineMap = lineMap;
    }

    private void setPointHashMap() {
        HashMap<Integer, Point> pointMap = new HashMap();
        for (int i = 0; i < planeSize(); i++) {
            pointMap.put(i, getElement(i));
        }
        this.pointMap = pointMap;
    }

    public HashMap<Integer, Integer> intersectionMap() {
        int lines = lineMap.size();
        HashMap<Integer, Integer> pointsInLine = new HashMap<>();
        for (int i = 0; i < lines; i++) {
            int intersections = 0;
            for (int j = 0; j < planeSize(); j++) {
                if (intersection(lineMap.get(i), pointMap.get(j))) {
                    intersections++;
                }
            }
            pointsInLine.put(i, intersections - 2);
        }
        return pointsInLine;
    }

    public void saveToFile(String file) throws IOException {
        FileWriter fstream = new FileWriter("./data/lab10/" + file);
        BufferedWriter out = new BufferedWriter(fstream);
        HashMap<Integer, Integer> intersectionMap = intersectionMap();
        Iterator<Map.Entry<Integer, Integer>> it = intersectionMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Integer> pairs = it.next();
            if (pairs.getValue() > 0) {
                Line line = lineMap.get(pairs.getKey());
                out.write(line.toString() + " intersects " + pairs.getValue() + " more points\n");
            }
        }
        out.close();
    }
}
