package by.mmf.sharubapv.lab4.textfile;

import by.mmf.sharubapv.lab4.exceptions.DirectoryException;
import by.mmf.sharubapv.lab4.exceptions.FileException;

import java.util.Objects;

public class TextFile extends File {
    public String content;
    {
        this.content = "";
    }

    public TextFile() {
        super(Type.txt);
    }

    public TextFile(char disk,String[] folders, String name) throws DirectoryException {
        super(disk,folders, name, Type.txt);
    }
    public void rename(String name){
        this.name=name;
    }
    public void add(String str){
        content+=str;
    }
    public void remove(int count) throws FileException {
        if(count>content.length()) throw new FileException("Trying to delete more then exist!");
        if(count<=0) throw new FileException("Wrong count!");
        content=content.substring(0,content.length()-count);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TextFile textFile = (TextFile) o;
        return Objects.equals(content, textFile.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), content);
    }
    @Override
    public String toString(){
        return super.toString()+"\n"+content;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
