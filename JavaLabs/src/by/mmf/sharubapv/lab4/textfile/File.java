package by.mmf.sharubapv.lab4.textfile;

import by.mmf.sharubapv.lab4.exceptions.DirectoryException;

import java.util.Objects;

public class File extends Directory {
    enum Type{txt,png,jpeg,exe,bin,bat,java}
    String name;
    private Type type;
    File(Type type) {
        super();
        name="newfile";
        this.type=type;
    }

    public File(char disk,String[] folders, String name, Type type) throws DirectoryException {
        super(disk,folders);
        this.name = name;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        File file = (File) o;
        return name.equals(file.name) &&
                type == file.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, type);
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "File{"+ super.toString() + name + '.'+type+'}';
    }
}
