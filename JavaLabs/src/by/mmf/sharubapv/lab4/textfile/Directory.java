package by.mmf.sharubapv.lab4.textfile;

import by.mmf.sharubapv.lab4.exceptions.DirectoryException;

import java.util.Arrays;
import java.util.Objects;

public class Directory {
    char disk;
    String[] folders;

    Directory() {
        disk = 'C';
        folders = new String[0];
    }

    Directory(char disk, String[] folders) throws DirectoryException {
        if (disk < 'A' || disk > 'Z')
            throw new DirectoryException("Wrong disk name!");
        for (String f : folders)
            if (f.isEmpty()) throw new DirectoryException("Wrong folders path!");
        this.folders = folders;
        this.disk = disk;

    }

    public void change(char disk, String[] folders) throws DirectoryException {
        if (disk < 'A' || disk > 'Z')
            throw new DirectoryException("Wrong disk name!");

        for (String f : folders)
            if (f.isEmpty()) throw new DirectoryException("Wrong folders path!");
        this.folders = folders;
        this.disk = disk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Directory directory = (Directory) o;
        return disk == directory.disk &&
                Arrays.equals(folders, directory.folders);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(disk);
        result = 31 * result + Arrays.hashCode(folders);
        return result;
    }

    @Override
    public String toString() {
        String dir = disk + "\\:";
        for (String s : folders)
            dir += s + '\\';
        return dir;
    }
}
