package by.mmf.sharubapv.lab4.textfile;

import by.mmf.sharubapv.lab4.exceptions.DirectoryException;

import java.util.Scanner;

public class MainTextFile {
    public static Scanner sc = new Scanner(System.in);

    public static TextFile newFile() throws DirectoryException {
        System.out.println("Enter disc:");
        char disk = sc.nextLine().charAt(0);
        System.out.println("Enter folders path(e.g. 1\\2\\3...):"); //    1\\2  1\2     1 2
        String path = sc.nextLine();
        String[] folders = path.split("\\\\");
        try {
            for (String fold : folders)
                if (fold.isEmpty()) throw new DirectoryException("Wrong PATH");
        } catch (DirectoryException e) {
            System.err.println(e.getMessage()+"\nDirectory was set to disk root");
            folders= new String[0];
        }
        System.out.println("Enter name:");
        String name = sc.nextLine();
        return new TextFile(disk, folders, name);


    }

    public static void main(String[] args) throws Throwable {
        try {
            TextFile file = newFile();
            int menu = 0;
            while (menu != 6) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                System.out.println("1)Replace content\n2)Add content\n3)Remove content\n4)Show\n5)Rename\n6)Delete");
                menu = sc.nextInt();
                sc.nextLine();
                switch (menu) {
                    case 1:
                        file.content = sc.nextLine();
                        break;
                    case 2:
                        file.add(sc.nextLine());
                        break;
                    case 3:
                        System.out.println("Enter number of symbols to remove:");
                        file.remove(sc.nextInt());
                        break;
                    case 4:
                        System.out.println(file.toString());
                        break;
                    case 5:
                        System.out.println("Enter new name:");
                        file.rename(sc.nextLine());
                        break;
                    case 6:
                        file.finalize();
                }
            }
        } catch (Throwable e) {
            System.err.println(e.getMessage() + "\nExited from program.");
            System.exit(1);
        }
    }
}
