package by.mmf.sharubapv.lab4.exceptions;

public class DirectoryException extends Exception {
    public DirectoryException(){ }
    public DirectoryException(String message,Throwable cause){
        super(message,cause);
    }
    public DirectoryException(String message){
        super(message);
    }
    public DirectoryException(Throwable cause){
        super(cause);
    }
}
