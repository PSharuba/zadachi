package by.mmf.sharubapv.lab4.exceptions;

public class TariffException extends Exception {
    public TariffException(){ }
    public TariffException(String message,Throwable cause){
        super(message,cause);
    }
    public TariffException(String message){
        super(message);
    }
    public TariffException(Throwable cause){
        super(cause);
    }
}
