package by.mmf.sharubapv.lab4.exceptions;

public class FileException extends Exception {
    public FileException(){ }
    public FileException(String message,Throwable cause){
        super(message,cause);
    }
    public FileException(String message){
        super(message);
    }
    public FileException(Throwable cause){
        super(cause);
    }
}
