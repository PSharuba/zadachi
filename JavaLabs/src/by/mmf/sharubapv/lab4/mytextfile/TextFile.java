package by.mmf.sharubapv.lab4.mytextfile;

import by.mmf.sharubapv.lab4.exceptions.DirectoryException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class TextFile implements Serializable {
    private String path;
    private String name;

    public TextFile() throws IOException {
        this.path = "./data/lab4/";
        this.name = "newTextFile";
        FileWriter fstream = new FileWriter(path + name + ".txt");
        System.out.println("Created new text file " + name + ".txt");
        fstream.close();
    }

    public TextFile(String path, String name) throws IOException {
        ArrayList<String> splpath = new ArrayList<>();
        for (String s : path.split("/")) {
            if (!s.isEmpty()) splpath.add(s);
        }
        try {
            checkPath(splpath);
        } catch (DirectoryException e) {
            e.printStackTrace();
        }
        StringBuilder pathBuilder = new StringBuilder();
        for (String s : splpath) {
            pathBuilder.append(s).append("/");
        }
        path = pathBuilder.toString();
        this.path = path;
        this.name = name;
        FileWriter fstream = new FileWriter(path + name + ".txt");
        System.out.println("Created new text file " + name + ".txt");
        fstream.close();
    }

    public TextFile(String name) throws IOException {
        this.path = "./data/lab4/";
        this.name = name;
        FileWriter fstream = new FileWriter(path + name + ".txt");
        System.out.println("Created new text file " + name + ".txt");
        fstream.close();
    }

    public void setContent(String newContent) throws IOException {
        FileWriter fstream = new FileWriter(path + name + ".txt");
        BufferedWriter out = new BufferedWriter(fstream);
        out.write(newContent);
        out.close();
        System.out.println("Saved new content to file " + name + ".txt");
    }

    public void addContent(String newContent) throws IOException {
        FileWriter fstream = new FileWriter(path + name + ".txt", true);
        BufferedWriter out = new BufferedWriter(fstream);
        out.write(newContent);
        out.close();
        System.out.println("Added new content to file " + name + ".txt");
    }

    public String getContent() throws IOException {
        return new String(Files.readAllBytes(Paths.get(path + name + ".txt")), StandardCharsets.UTF_8);
    }

    public void removeContent(int i) throws IOException {
        String content = getContent();
        if (content.length() < i) {
            setContent("");
            System.out.println("Content removed.");
        } else {
            setContent(content.substring(0, content.length() - i));
            System.out.println("Removed " + i + " characters.");
        }
    }

    public void removeContent(int s, int count) throws IOException {
        String content = getContent();
        if (s < 0) s = 0;
        if (s > content.length() - 1) return;
        String substring = content.substring(0, s);
        if (count > content.length() - s) {
            setContent(substring);
            System.out.println("Removed " + (content.length() - s) + " characters.");
        } else {
            setContent(substring + content.substring(s + count));
            System.out.println("Removed " + count + " characters.");
        }
    }

    public void delete() {
        final File file = new File(path + name + ".txt");
        if (file.delete()) {
            System.out.println("File " + name + ".txt removed from:\n" + path + "\n");
        } else {
            System.out.println("Can't delete File");
        }
    }

    private void checkPath(ArrayList<String> path) throws DirectoryException {
        for (String s : path) {
            for (int i = 0; i < s.length(); i++)
                if (forbidden(s.charAt(i))) throw new DirectoryException("Forbidden symbol '" + s.charAt(i)+"' in path!");
        }
    }

    private boolean forbidden(char c) {
        switch (c) {
            case '\\':
            case ',':
            case ':':
            case '*':
            case '|':
            case '?':
            case '<':
            case '>':
                return true;
            default:
                return false;
        }
    }

    public static void main(String[] args) {
        try {
            TextFile test = new TextFile();
            test.setContent("Test success?\n");
            test.addContent("Probably.");
            test.removeContent(2);
            test.removeContent(1, 3);
            System.out.println(test.getContent());
            test.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
