package by.mmf.sharubapv.lab4.mobile;

import java.io.Serializable;

public class SocialTariff extends Tariff  implements Serializable {
    private int minutesInside;
    private int minutesOutside;
    private int mbInternet;
    public SocialTariff(){
        super("New social tariff",0,0);
        minutesInside=0;
        minutesOutside=0;
        mbInternet=0;
    }

    public SocialTariff(String name, double subscriptionFee, int users, int minutesInside, int minutesOutside, int mbInternet) {
        super(name, subscriptionFee, users);
        if(minutesInside>=0) {
            this.minutesInside = minutesInside;
        } else {
            System.out.println("Minutes inside <0");
            this.minutesInside=0;
        }
        if(minutesOutside>=0) {
            this.minutesOutside = minutesOutside;
        } else {
            System.out.println("Minutes outside <0");
            this.minutesOutside=0;
        }
        if(mbInternet>=0) {
            this.mbInternet = mbInternet;
        } else {
            System.out.println("MB internet <0");
            this.mbInternet=0;
        }
    }

    public int getMinutesInside() {
        return minutesInside;
    }

    public void setMinutesInside(int minutesInside) {
        this.minutesInside = minutesInside;
    }

    public int getMinutesOutside() {
        return minutesOutside;
    }

    public void setMinutesOutside(int minutesOutside) {
        this.minutesOutside = minutesOutside;
    }

    public int getMbInternet() {
        return mbInternet;
    }

    public void setMbInternet(int mbInternet) {
        this.mbInternet = mbInternet;
    }

    public double compareTo(SocialTariff s){
        return this.subscriptionFee-s.getSubscriptionFee();
    }
    @Override
    public String toString(){
        return "Social Tariff '"+name+
                "'\nSubscription fee: " +subscriptionFee+
                "\nMinutes inside: "+minutesInside+
                "\nMinutes outside: "+minutesOutside+
                "\nMB internet: "+mbInternet+
                "\nUsers amount: "+users;
    }
}
