package by.mmf.sharubapv.lab4.mobile;

import java.io.Serializable;

public class FreeTariff extends Tariff implements Serializable {
    private double priceOfMinuteInside;
    private double priceOfMinuteOutside;
    private double priceOfMbInternet;
    public FreeTariff(){
        super("New free tariff",0,0);
        priceOfMinuteInside=0;
        priceOfMinuteOutside=0;
        priceOfMbInternet=0;
    }

    public FreeTariff(String name, int users, double priceOfMinuteInside, double priceOfMinuteOutside, double priceOfMbInternet) {
        super(name, 0, users);
        if(priceOfMinuteInside>=0) {
            this.priceOfMinuteInside = priceOfMinuteInside;
        }else{
            System.out.println("Price of minute inside <0");
            this.priceOfMinuteInside=0;
        }
        if(priceOfMinuteOutside>=0) {
            this.priceOfMinuteOutside = priceOfMinuteOutside;
        }else{
            System.out.println("Price of minute outside <0");
            this.priceOfMinuteOutside=0;
        }
        if(priceOfMbInternet>=0) {
            this.priceOfMbInternet = priceOfMinuteInside;
        }else{
            System.out.println("Price of mb internet <0");
            this.priceOfMbInternet=0;
        }
    }

    public double getPriceOfMinuteInside() {
        return priceOfMinuteInside;
    }

    public void setPriceOfMinuteInside(double priceOfMinuteInside) {
        this.priceOfMinuteInside = priceOfMinuteInside;
    }

    public double getPriceOfMinuteOutside() {
        return priceOfMinuteOutside;
    }

    public void setPriceOfMinuteOutside(double priceOfMinuteOutside) {
        this.priceOfMinuteOutside = priceOfMinuteOutside;
    }

    public double getPriceOfMbInternet() {
        return priceOfMbInternet;
    }

    public void setPriceOfMbInternet(double priceOfMbInternet) {
        this.priceOfMbInternet = priceOfMbInternet;
    }

    @Override
    public String toString(){
        return "Free Tariff '"+name+"'\nPrice of minute inside: "+priceOfMinuteInside+
                "\nPrice of minute outside: "+priceOfMinuteOutside+
                "\nPrice of mb internet: "+priceOfMbInternet+
                "\nUsers amount: "+users;
    }
}
