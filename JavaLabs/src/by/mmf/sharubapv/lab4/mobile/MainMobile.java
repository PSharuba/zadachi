package by.mmf.sharubapv.lab4.mobile;


import by.mmf.sharubapv.lab4.exceptions.TariffException;
import by.mmf.sharubapv.lab9.b.ObjectWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class MainMobile {
    public static ArrayList<FreeTariff> freeTariffs = new ArrayList<>();
    public static ArrayList<SocialTariff> socialTariffs = new ArrayList<>();
    public static Scanner in = new Scanner(System.in);

    private static SocialTariff[] sortSocial(ArrayList<SocialTariff> so) {
        SocialTariff[] arr = so.toArray(new SocialTariff[0]);
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j].getSubscriptionFee() > arr[j + 1].getSubscriptionFee()) {
                    SocialTariff tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return arr;
    }

    public static void searchTariff() throws TariffException {
        System.out.println("Enter desired subscription fee (where x=0 -> Free tariff)\n");
        double fee = in.nextDouble();
        if (fee < 0) throw new TariffException("Fee can't be less then 0");
        boolean empty = true;
        if (fee > 0) {
            System.out.println("Enter desired count of minutes inside and outside and mb's of internet:");
            int inside = in.nextInt();
            int outside = in.nextInt();
            int mb = in.nextInt();
            if (inside < 0 || outside < 0 || mb < 0)
                throw new TariffException("Minutes or internet amount can't be less then 0");
            for (SocialTariff s : socialTariffs) {
                if (s.subscriptionFee <= fee && s.getMbInternet() >= mb && s.getMinutesInside() >= inside && s.getMinutesOutside() >= outside) {
                    System.out.println(s.toString());
                    empty = false;
                }
            }
            if (empty) System.out.println("\nTariffs not found!\n");
        } else {
            for (FreeTariff f : freeTariffs)
                System.out.println(f.toString());
        }
    }

    public static void sortTariffs(ArrayList<SocialTariff> so, ArrayList<FreeTariff> fr) {
        System.out.println("Free tariffs:\n");
        for (FreeTariff f : fr) {
            System.out.println(f.toString() + '\n');
        }
        System.out.println("Social tariffs");
        SocialTariff[] sorted = sortSocial(so);
        for (SocialTariff s : sorted) {
            System.out.println(s.toString() + '\n');
        }
    }

    public static SocialTariff addSocial() throws TariffException {
        System.out.println("Enter new Social tariff name:");
        String name = in.nextLine();
        System.out.println("Enter new Social tariff subscription fee, users amount, minutes inside and outside, mb's of internet");
        double subscriptionFee = in.nextDouble();
        int users = in.nextInt();
        int inside = in.nextInt();
        int outside = in.nextInt();
        int internet = in.nextInt();
        if (users < 0 || inside < 0 || outside < 0 || internet < 0) {
            throw new TariffException("Users, minutes or internet amount can't be less then 0");
        }
        return new SocialTariff(name, subscriptionFee, users, inside, outside, internet);
    }

    public static FreeTariff addFree() throws TariffException {
        System.out.println("Enter new Free tariff name:");
        String name = in.nextLine();
        System.out.println("Enter new Free tariff users amount,price for minute inside and outside,price for mb of internet");
        int users = in.nextInt();
        double inside = in.nextDouble();
        double outside = in.nextDouble();
        double internet = in.nextDouble();
        if (users < 0 || inside < 0 || outside < 0 || internet < 0)
            throw new TariffException("Users, minutes or internet price can't be less then 0");
        return new FreeTariff(name, users, inside, outside, internet);
    }

    public static void saveToFiles() throws IOException {
        FileWriter fstream = new FileWriter("./data/lab4/freeTariffs.txt");
        BufferedWriter out = new BufferedWriter(fstream);
        Iterator<FreeTariff> itF = freeTariffs.iterator();
        while (itF.hasNext()) {
            FreeTariff next = itF.next();
            out.write(next.toString() + "\n\n============\n\n");
        }
        out.close();
        fstream = new FileWriter("./data/lab4/socialTariffs.txt");
        out = new BufferedWriter(fstream);
        Iterator<SocialTariff> itS = socialTariffs.iterator();
        while (itS.hasNext()) {
            SocialTariff next = itS.next();
            out.write(next.toString() + "\n\n============\n\n");
        }
        out.close();
    }

    public static void main(String[] args) {
        try {
            System.out.println("Add at least one tariff to continue");
            int menuFlag = -1;
            while (menuFlag < 0 || menuFlag > 2) {
                System.out.println("1)Add new Social tariff\n" +
                        "2)Add new Free tariff\n" +
                        "0)Exit");
                menuFlag = in.nextInt();
                if (menuFlag < 0 || menuFlag > 2) System.out.println("Wrong action\n-------------\n");
                in.nextLine();
            }
            switch (menuFlag) {
                case 0:
                    System.exit(0);
                case 1:
                    socialTariffs.add(addSocial());
                    System.out.println("Added new Social tariff:\n" + socialTariffs.get(0).toString());
                    break;
                case 2:
                    freeTariffs.add(addFree());
                    System.out.println("Added new Free tariff:\n" + freeTariffs.get(0).toString());
                    break;
            }
            {
                socialTariffs.add(new SocialTariff("Social 1000", 100, 12341, 100, 100, 1000));
                socialTariffs.add(new SocialTariff("Social 500", 500, 121451, 200, 200, 500));
                socialTariffs.add(new SocialTariff("Social 100", 50, 3542, 100, 100, 100));
                freeTariffs.add(new FreeTariff("Free 30+", 36341, 10, 10, 5));
                freeTariffs.add(new FreeTariff("Free 40+", 8623, 5, 5, 10));
                freeTariffs.add(new FreeTariff("Free 50+", 9863, 4, 4, 10));
            }
            while (true) {
                System.out.println("\n\n\n\n\n\tMENU\n" +
                        "1)Add new Social tariff\n" +
                        "2)Add new Free tariff\n" +
                        "3)Sort tariffs\n" +
                        "4)Search fitting tariff\n" +
                        "5)Save to files\n" +
                        "0)Exit"
                );
                menuFlag = in.nextInt();
                switch (menuFlag) {
                    default:
                        System.out.println("Wrong action\n-------------\n");
                        break;
                    case 1:
                        socialTariffs.add(addSocial());
                        System.out.println("Added new Social tariff:\n" + socialTariffs.get(0).toString());
                        break;
                    case 2:
                        freeTariffs.add(addFree());
                        System.out.println("Added new Free tariff:\n" + freeTariffs.get(0).toString());
                        break;
                    case 3:
                        sortTariffs(socialTariffs, freeTariffs);
                        break;
                    case 4:
                        searchTariff();
                        break;
                    case 5:
                        try {
                            saveToFiles();
                            System.out.println("Saved!");
                            /*for(int i=0;i<socialTariffs.size();i++) {
                                new ObjectWriter<SocialTariff>().writeObject("SocialTariff#"+i,socialTariffs.get(i));
                            }
                            for(int i=0;i<freeTariffs.size();i++) {
                                new ObjectWriter<FreeTariff>().writeObject("FreeTariff#"+i,freeTariffs.get(i));
                            }*/
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 0:
                        System.out.println("Have a nice day!");
                        System.exit(0);
                }
            }
        } catch (TariffException e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        } catch (Throwable e) {
            System.err.println(e.getMessage());
            System.exit(-2);
        }
    }
}
