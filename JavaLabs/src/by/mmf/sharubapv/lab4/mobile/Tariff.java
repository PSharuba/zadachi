package by.mmf.sharubapv.lab4.mobile;

import java.io.Serializable;

public class Tariff implements Serializable {
    protected String name;
    protected double subscriptionFee;
    protected int users;
    public Tariff(){
        name="New test Tariff";
        subscriptionFee=0;
        users=0;
    }

    public Tariff(String name, double subscriptionFee, int users) {
        this.name = name;
        if(subscriptionFee>=0) {
            this.subscriptionFee = subscriptionFee;
        } else {
            System.out.println("Alert! Subscription fee <0");
            this.subscriptionFee=0;
        }
        if (users>=0){
            this.users=users;
        } else {
            System.out.println("Alert! Users amount <0");
            this.users=0;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setsubscriptionFee(double subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }

    @Override
    public String toString(){
        return "Tariff "+name+"\nSubscription fee: "+subscriptionFee+"; Users amount: "+users;
    }
}
