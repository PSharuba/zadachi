package by.mmf.sharubapv.lab3.airplanes;

public class Airplane {
    enum Type{Transport,Passanger,Private}
    enum DaysOfWeek{Monday,Tuesday,Wednesday,Thursday,Friday}
    private static double _IDcounter=0;
    private double id;
    private Type type;
    private DaysOfWeek day;
    private int hours;
    private int minutes;
    private String destination;

    public Airplane(Type type, DaysOfWeek day, int hours, int minutes, String destination) {
        setId();
        this.type = type;
        this.day = day;
        this.hours = hours;
        this.minutes = minutes;
        this.destination = destination;
    }

    public Type getType() {
        return type;
    }

    public DaysOfWeek getDay() {
        return day;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }
    public void setHours(int hours){
        this.hours=hours;
    }
    public void setMinutes(int minutes){
        this.minutes=minutes;
    }

    public String getDestination() {
        return destination;
    }

    private void setId(){
        id=_IDcounter++;
    }
    @Override
    public String toString(){
        return(
                "Airplane:\t"+type+
                        "\nId: "+id+
                        "\nDestination: "+destination+
                        "\nDay: "+ day+
                        "\nTime:"+hours+":"+minutes+"\n"
                );
    }


}
