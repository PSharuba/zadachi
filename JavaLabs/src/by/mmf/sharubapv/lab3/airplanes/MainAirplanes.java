package by.mmf.sharubapv.lab3.airplanes;

import java.util.ArrayList;
import java.util.Scanner;

public class MainAirplanes {
    public static Scanner sc = new Scanner(System.in);

    public static ArrayList<Airplane> insertNew(ArrayList<Airplane> list) {
        Airplane.Type type;
        Airplane.DaysOfWeek day;
        int hours;
        int minutes;
        String destination;
        System.out.println("Choose type:\n" +
                "1)Passanger\n" +
                "2)Transport\n" +
                "3)Private"
        );
        switch (sc.nextInt()) {
            case 1:
                type = Airplane.Type.Passanger;
                break;
            case 2:
                type = Airplane.Type.Transport;
                break;
            default:
                System.out.println("Wrong type. Set to 'Private'.");
            case 3:
                type = Airplane.Type.Private;
                break;
        }
        System.out.println("Enter day number:\n" +
                "(1 - Monday to 5 - Friday)"
        );
        switch (sc.nextInt()) {
            default:
                System.out.println("Wrong day. Set to 'Monday'.");
            case 1:
                day = Airplane.DaysOfWeek.Monday;
                break;
            case 2:
                day = Airplane.DaysOfWeek.Tuesday;
                break;
            case 3:
                day = Airplane.DaysOfWeek.Wednesday;
                break;
            case 4:
                day = Airplane.DaysOfWeek.Thursday;
                break;
            case 5:
                day = Airplane.DaysOfWeek.Friday;
                break;
        }
        System.out.println("Enter time ( hh mm ):");
        hours = sc.nextInt();
        minutes = sc.nextInt();
        if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0) {
            System.out.println("Wrong time. Time set to 12:00");
            hours = 12;
            minutes = 0;
        }
        sc.nextLine();
        System.out.println("Enter destination:");
        destination = sc.nextLine();
        list.add(new Airplane(type, day, hours, minutes, destination));
        return list;
    }

    public static void main(String[] args) {
        System.out.println("Adding new airplane.\n");
        ArrayList<Airplane> flights = new ArrayList<>();
        flights.add(new Airplane(Airplane.Type.Transport, Airplane.DaysOfWeek.Monday, 4, 00, "Moskow"));
        flights.add(new Airplane(Airplane.Type.Passanger, Airplane.DaysOfWeek.Monday, 13, 05, "Warshaw"));
        flights.add(new Airplane(Airplane.Type.Transport, Airplane.DaysOfWeek.Thursday, 9, 45, "Moskow"));
        flights.add(new Airplane(Airplane.Type.Passanger, Airplane.DaysOfWeek.Thursday, 19, 45, "Berlin"));
        flights.add(new Airplane(Airplane.Type.Private, Airplane.DaysOfWeek.Tuesday, 9, 30, "Warshaw"));
        flights.add(new Airplane(Airplane.Type.Private, Airplane.DaysOfWeek.Wednesday, 10, 55, "Kiev"));
        flights.add(new Airplane(Airplane.Type.Passanger, Airplane.DaysOfWeek.Friday, 10, 55, "Kiev"));
        flights = insertNew(flights);
        System.out.println("\n==========\nFlights to Moskow:");
        for (Airplane flight : flights) {
            if (flight.getDestination().equals("Moskow")) {
                System.out.println(flight.toString());
            }
        }
        System.out.println("==========\nFlights at Thursday:");
        for (Airplane flight : flights) {
            if (flight.getDay() == Airplane.DaysOfWeek.Thursday) {
                System.out.println(flight.toString());
            }
        }
        System.out.println("==========\nEnter desired day: ");
        String desiredDay = sc.nextLine();
        System.out.println("Enter time (hh mm):");
        int[] desiredTime = new int[2];
        desiredTime[0] = sc.nextInt();
        desiredTime[1] = sc.nextInt();
        System.out.printf(
                "==========\nFlights at %s from %02d:%02d\n", desiredDay, desiredTime[0], desiredTime[1]);
        for (Airplane flight : flights) {
            if (flight.getHours() >= desiredTime[0] && flight.getMinutes() >= desiredTime[1] && flight.getDay() == Airplane.DaysOfWeek.valueOf(desiredDay)) {
                System.out.println(flight.toString());
            }
        }
    }
}
