package by.mmf.sharubapv.lab3.vectors;

public class Vector {
    private double[] vector;
    private Vector(){
        vector=new double[1];
        vector[0]=0;
    }
    public Vector(double[] args){
        vector=args;
    }
    public double module(){
        double sum=0;
        for(double arg: vector){
            sum+=Math.pow(arg,2);
        }
        return Math.sqrt(sum);
    }
    public double scalarProduct(Vector vct){
        if(vct.vector.length!=vector.length) {
            System.out.println("\nNot equal size\n");
            return 0;
        }
        double sum=0;
        for(int i=0;i<vector.length;i++){
            sum+=vct.vector[i]*vector[i];
        }
        return sum;
    }
    public Vector add(Vector vect){
        double[] vct = new double[vect.length()];
        for (int i=0; i<vect.length();i++)
            vct[i]=vector[i]+vect.vector[i];
        return new Vector(vct);
    }
    public Vector substract(Vector vect){
        double[] vct = new double[vect.length()];
        for (int i=0; i<vect.length();i++)
            vct[i]=vector[i]-vect.vector[i];
        return new Vector(vct);
    }
    public void constMult(double constant){
        for (int i=0;i<vector.length;i++)
            vector[i]*=constant;
    }
    public boolean colinear(Vector vct){
        if(vct.vector.length!=vector.length) {
            System.out.println("\nNot equal size\n");
            return false;
        }
        double[] koef=new double[vector.length];
        for (int i=0;i<vector.length;i++)
            koef[i]=vector[i]/vct.vector[i];
        for(double k: koef) {
            if (k != koef[0]) {
                return false;
            }
        }
        return true;
    }
    private int length(){
        return vector.length;
    }
    public boolean perpendicular(Vector vct){
        if(vct.vector.length!=vector.length) {
            System.out.println("\nNot equal size\n");
            return false;
        }
        return (scalarProduct(vct)==0);
    }
    @Override
    public String toString(){
        String str="[";
        for (int i=0;i<vector.length-1;i++)
            str +=" "+vector[i]+" ,";
        str+=vector[vector.length-1]+" ]";
        return str;
    }

}
