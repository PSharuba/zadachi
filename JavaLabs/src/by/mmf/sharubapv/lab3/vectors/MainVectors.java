package by.mmf.sharubapv.lab3.vectors;

import java.util.Scanner;

public class MainVectors {
    public static Scanner in=new Scanner(System.in);
    public static int size;
    public static void main(String[] args) {
        System.out.println("Enter vectors size:");
        size=in.nextInt();
        System.out.println("Enter number of vectors (>=2):");
        int number;
        while ((number=in.nextInt())<2);
        Vector[] vectors=new Vector[number];
        for (int i=0;i<number;i++){
            double[] vct = new double[size];
            System.out.println("Enter vector (like 0 1,2 -0,3 1 ...)");
            for (int j=0;j<size;j++)
                vct[j]=in.nextDouble();
            vectors[i]=new Vector(vct);
        }
        System.out.println("Your vectors:");
        for(Vector vct: vectors)
            System.out.println(vct.toString());
        System.out.print("\n"+vectors[0].toString()+" and "+vectors[1].toString()+" are");
        if(vectors[1].colinear(vectors[0])) System.out.print(" colinear\n\n");
        else System.out.print("n't colinear\n\n");
        System.out.print(vectors[1].toString()+" and "+vectors[0].toString()+" are");
        if(vectors[0].perpendicular(vectors[1])) System.out.print(" perpendicular\n\n");
        else System.out.print("n't perpendicular\n\n");
        System.out.println(vectors[0].toString()+"+"+vectors[1].toString()+" = "+vectors[0].add(vectors[1]).toString());
        System.out.println(vectors[0].toString()+"-"+vectors[1].toString()+" = "+vectors[0].substract(vectors[1]).toString());
        System.out.println(vectors[0].toString()+"*"+vectors[1].toString()+" = "+vectors[0].scalarProduct(vectors[1]));
        System.out.println("Vector "+vectors[0].toString()+" has length "+vectors[0].module());
    }
}
