package by.mmf.sharubapv.lab7.c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Lab7c {
    private String word;
    private ArrayList<String> movs;
    {
        movs=new ArrayList<>();
    }
    Lab7c (){
        word="JAVA";
    }
    Lab7c(String word){
        String[] tmp=word.split("\\s");
        if(tmp.length>1) System.out.println("Took the first word '"+tmp[0]+"'");
        this.word=tmp[0];
        this.word=this.word.trim();
    }
    public ArrayList<String> move(){
        String tmp=word;
        for(int i=0;i<word.length();i++) {
            tmp=tmp.substring(1)+tmp.charAt(0); //avaj vaja ajav
            movs.add(tmp);
        }
        return movs;
    }
    public String Bor_Uil(){
        Collections.sort(movs, String.CASE_INSENSITIVE_ORDER);
        String result="";
        for (String s:movs){
            result+=s.charAt(s.length()-1);
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter word:");
        String word=sc.nextLine();
        Lab7c algoritm=new Lab7c(word);
        ArrayList<String> movs=algoritm.move();
        System.out.println("\nWord movings:\n");
        for (String s:movs){
            System.out.println(s);
        }
        System.out.println("\nResult: "+algoritm.Bor_Uil());
    }
}
