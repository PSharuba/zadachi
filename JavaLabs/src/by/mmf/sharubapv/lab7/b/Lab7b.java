package by.mmf.sharubapv.lab7.b;

import java.util.Scanner;

public class Lab7b {
    private String text;

    public Lab7b(String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void format(int len) {
        StringBuilder sb = new StringBuilder();
        String[] strArr = text.split("\\s");
        for (int i = 0; i < strArr.length; i++) {

            if (!(strArr[i].length() >= len  && !isVowel(strArr[i])) || strArr[i].length() < len) {
                sb.append(strArr[i]).append(" ");
            }
        }
        text = sb.toString().trim();
        if(text.charAt(text.length()-1)!='.')
            text+='.';
    }
    private boolean isVowel(String incomingText) {
        switch (Character.toLowerCase(incomingText.charAt(0))) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
            default:
                return false;

        }
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Lab7b main=new Lab7b("Text from which you need to delete all" +
                " words longer then N characters starting with consonants.");
        System.out.println("Text before formatting:\n" +
                main.getText()+
                "\n\nEnter length of words to search for:");
        main.format(sc.nextInt());
        System.out.println("Text after formatting:\n"+main.getText());
    }
}
