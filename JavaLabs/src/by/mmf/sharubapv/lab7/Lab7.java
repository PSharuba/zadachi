package by.mmf.sharubapv.lab7;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lab7 {
    public static final Pattern REGEX_WORD =
            Pattern.compile("[А-Яа-яA-Za-zЁё].+?(\\.|\\t|\\?|!|,|\\s|;|:)");
    public static final Pattern REGEX_PARAGRAPH =
            Pattern.compile("[А-ЯA-ZЁ].+?\\t");
    public static final Pattern REGEX_SENTENCE =
            Pattern.compile("[А-ЯA-ZЁ].+?(\\t|([?.!]\\s))");
    public static final Pattern REGEX_COMMAND =
            Pattern.compile("[A-Za-z].+?;|}");


    private String text;

    Lab7() {
        text = "\tФорматирование строк\t" +
                "Для создания форматированного текстового вывода предназначен класс java.util.Formatter. " +
                "Этот класс обеспечивает преобразование формата, позволяющее выводить числа, строки, время " +
                "и даты в любом необходимом разработчику виде.\t" +
                "В классе Formatter объявлен метод format(), который преобразует переданные в него параметры " +
                "в строку заданного формата и сохраняет в объекте типа Formatter. Аналогичный метод объявлен у " +
                "классов PrintStream и PrintWriter. Кроме того у этих классов объявлен метод printf() с параметрами, " +
                "идентичными параметрам метода format(), который осуществляет форматированный вывод в поток, тогда как " +
                "метод format() сохраняет изменения в объекте типа Formatter. Таким образом, метод printf() " +
                "автоматически использует возможности класса Fomatter и подобен функции printf() языка С.\t" +
                "Класс Formatter преобразует двоичную форму представления данных в форматированный текст. Он сохраняет " +
                "форматированный текст в буфере, содержимое которого можно получить в любой момент. Можно предоставить " +
                "классу Formatter автоматическую поддержку этого буфера либо задать его явно при создании объекта. Существует " +
                "возможность сохранения буфера класса Formatter в файле.\t";
    }

    public void parser() {
        try {
            System.out.println("Words:");
            Matcher matcher = REGEX_WORD.matcher(text);
            while (matcher.find()) {
                System.out.println(text.substring(matcher.start(), matcher.end()));
            }
            matcher = REGEX_PARAGRAPH.matcher(text);
            System.out.println("\nParagraphs:");
            while (matcher.find()) {
                System.out.println(text.substring(matcher.start(), matcher.end()) + '\n');
            }
            matcher = REGEX_SENTENCE.matcher(text);
            System.out.println("\nSentences:");
            while (matcher.find()) {
                System.out.println(text.substring(matcher.start(), matcher.end()));
            }
        } catch (Throwable e) {
            System.out.println("\n" + e.getMessage());
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        Lab7 m = new Lab7();
        m.parser();
    }
}
