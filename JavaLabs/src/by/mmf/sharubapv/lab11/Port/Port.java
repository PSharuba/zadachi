package by.mmf.sharubapv.lab11.Port;


import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

public class Port {
    Queue<Pier> piers;
    Queue<Goods> goods;
    int capacity;

    public Port(int pierCount, int capacity) {
        goods = new LinkedBlockingDeque<>(capacity);
        piers = new LinkedBlockingDeque<>(pierCount);
        this.capacity = capacity;
        for (int i = 0; i < pierCount; i++)
            piers.add(new Pier("Pier " + (i + 1)));
        int num=(int)(Math.random()*capacity);
        for (int i = 0; i < num; i++) {
            goods.add(new Goods());
        }
    }

    public boolean addGoods(Goods item) {
        if (goods.size() >= capacity) return false;
        return goods.add(item);
    }

    public boolean takeGoods(Queue<Goods> storage) {
        if (goods.size() <= 0) return false;
        return storage.add(goods.poll());
    }

    public Pier getPier() {
        Pier taken = piers.poll();
        while (taken == null) {
            try {
                System.out.println("There aren't any free piers. Waiting...");
                Thread.sleep(1000);
                taken = piers.poll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return taken;
    }

    public void releasePier(Pier pier) {
        piers.add(pier);
    }

    public static void main(String[] args) {
        Port port = new Port(4, 50);
        ArrayList<Thread> ships = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ships.add(new Thread(new Ship("ship " + (i+1), port, 30)));
        }
        for (Thread t : ships)
            t.start();
    }
}
