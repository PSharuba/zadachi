package by.mmf.sharubapv.lab11.Port;

public class Goods {
    private static int NUMBER_COUNTER=1;
    int number;

    public Goods() {
        number = NUMBER_COUNTER++;
    }

    public int getNumber() {
        return number;
    }
}
