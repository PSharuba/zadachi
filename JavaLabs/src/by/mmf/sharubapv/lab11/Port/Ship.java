package by.mmf.sharubapv.lab11.Port;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;

public class Ship implements Runnable {
    String name;
    Port port;
    Queue<Goods> storage;
    Pier pier;
    int capacity;

    public Ship(String name, Port port, int capacity) {
        this.name = name;
        this.port = port;
        storage = new LinkedBlockingDeque<>(capacity);
        this.capacity = capacity;
        int num=(int)(Math.random()*capacity);
        for (int i = 0; i < num; i++) {
            storage.add(new Goods());
        }
    }

    public void run() {
        try {
            int unl = 0;
            int l = 0;
            pier = port.getPier();
            System.out.println("Ship " + name + " taken " + pier.name + "\n========================");
            while (unload()) {
                unl++;
                System.out.println("Ship " + name + " unloaded cargo.");
                Thread.sleep(new Random(100).nextInt(500));
            }
            System.out.println("Ship " + name + " unloaded " + unl + " crates.");
            Thread.sleep(100);
            while (load()) {
                l++;
                System.out.println("Ship " + name + " loaded cargo.");
                Thread.sleep(new Random(100).nextInt(500));
            }
            System.out.println("Ship " + name + " loaded " + l + " crates.");
            port.releasePier(pier);
            System.out.println("Ship " + name + " released " + pier.name + "\n========================");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean unload() {
        if (storage.size() <= 0) return false;
        return port.addGoods(storage.poll());
    }

    private boolean load() {
        if (storage.size() >= capacity) return false;
        return port.takeGoods(storage);
    }
}
