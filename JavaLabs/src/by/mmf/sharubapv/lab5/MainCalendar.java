package by.mmf.sharubapv.lab5;

import java.util.Scanner;

public class MainCalendar {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        CalendarYear newYear = new CalendarYear();
        boolean flag = true;
        while (flag) {
            System.out.println("1)Добавить праздник\n" +
                    "2)Посмотреть праздники\n" +
                    "3)Проверить день\n" +
                    "0)Выход\n");
            switch (sc.nextInt()) {
                case 1:
                    newYear.newHoliday();
                    break;
                case 2:
                    System.out.println(newYear.toString());
                    break;
                case 3:
                    newYear.isHoliday();
                    break;
                default:
                    flag = false;
                    break;
            }
        }
        System.out.println("Thanks for using our calendar!");
    }
}
