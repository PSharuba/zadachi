package by.mmf.sharubapv.lab5;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class CalendarYear {
    public Scanner sc = new Scanner(System.in);
    private int year;
    private boolean leap;
    private ArrayList<Holiday> holidaysArray = new ArrayList<>();

    enum Type {Official, Non_Official}

    public CalendarYear() {
        holidaysArray.add(new Holiday("New Year", 1, 1, true, Type.Official));
        holidaysArray.add(new Holiday("Victory day", 5, 9, true, Type.Official));
        holidaysArray.add(new Holiday("Defenders day", 2, 23, false, Type.Official));
        holidaysArray.add(new Holiday("Mike's birthday", 12, 4, false, Type.Non_Official) {
            public void celebrate() {
                System.out.println("You celebrated Mike's birthday!\n");
            }
        });
        holidaysArray.add(new Holiday("Women's day", 3, 8, true, Type.Official));
        System.out.println("Enter year:");
        year = sc.nextInt();
        System.out.println("Is it a leap year?\n1)y\n2)n");
        leap = (sc.nextInt() == 1);
    }

    public static class day{
        int month;
        int day;
        day(){
            month=1;
            day=1;
        }
    }

    public void newHoliday() {
        holidaysArray.add(new Holiday());
        System.out.println("Holiday added");
    }

    public class Holiday {
        private String name;
        private int month;
        private int day;
        private boolean dayOff;
        Type type;

        public Holiday() {
            setHoliday();
        }

        public Holiday(String name, int month, int day, boolean dayOff, Type type) {
            this.name = name;
            this.month = month;
            this.day = day;
            this.dayOff = dayOff;
            this.type = type;
        }

        public void setHoliday() {
            sc.nextLine();
            month = -1;
            day = -1;
            System.out.println("Enter holiday name:");
            name = sc.nextLine();
            System.out.println("Enter day and month");
            while (!validDate(day, month)) {
                day = sc.nextInt();
                month = sc.nextInt();
            }
            System.out.println("Is it a day-off?\n1)y\n2)n");
            dayOff = (sc.nextInt() == 1);
            System.out.println("What type of holiday it is?\n1)Official\n2)Non-Official");
            if (sc.nextInt() == 1) type = Type.Official;
            else type = Type.Non_Official;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Holiday holiday = (Holiday) o;
            return month == holiday.month &&
                    day == holiday.day &&
                    dayOff == holiday.dayOff &&
                    name.equals(holiday.name) &&
                    type == holiday.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, month, day, dayOff);
        }

        @Override
        public String toString() {
            return ("Name: " + name +
                    "\nDate: " + day + " / " + month +
                    "\nDay-off: " + dayOff +
                    "\nType: " + type +
                    "\n\n");
        }
    }

    private boolean validDate(int day, int month) {
        Integer[] longM = new Integer[]{1, 3, 5, 7, 8, 10, 12};
        if (month > 0 && month < 13) {
            if (includes(longM, month))
                return (day > 0 && day < 32);
            if (day > 0 && day < 31) {
                if (month != 2) return true;
                else if (leap && day < 30) return true;
                else return (day < 29);
            }
        }
        return false;
    }

    private boolean includes(Object[] arr, Object val) {
        for (Object v : arr)
            if (val.equals(v)) return true;
        return false;
    }

    public void isHoliday() {
        System.out.println("Enter day and month");
        int day = sc.nextInt();
        int month = sc.nextInt();
        if (!validDate(day, month)) {
            System.out.println("Invalid date!");
            return;
        }
        for (Holiday h : holidaysArray)
            if (h.day == day && h.month == month) {
                System.out.println("It is a holiday!");
                if (h.dayOff) System.out.println("And it is a day-off!\n");
                else System.out.println("But it isn't a day-off.\n");
                return;
            }
        System.out.println("It isn't a holiday.\n");
    }

    @Override
    public String toString() {
        String str = "Year: " + year +
                "\nLeap: " + leap + "\n\n";
        for (Holiday h : holidaysArray)
            str += h.toString();
        return str;

    }
}
